package com.example.taxestimator.config;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class WebConfigTest {
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	@Test
    public void bCryptPasswordEncoderShouldBeInjected() {
		assertNotNull("bCryptPasswordEncoder should not be null", bCryptPasswordEncoder);
    }

}
