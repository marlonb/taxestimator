package com.example.taxestimator.config;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.taxestimator.calculator.TaxCalculator;
import com.example.taxestimator.models.TaxEstimatorRequest;
import com.example.taxratesmodels.models.TaxRateYear;
import com.example.taxratesmodels.models.TaxRateYearRange;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ServiceProviderTest {
	
	@Autowired
	private BeanFactory beanFactory;
	
	TaxEstimatorRequest taxEstimatorRequest;
	
    @Before
    public void setup() {
    	
    	taxEstimatorRequest = new TaxEstimatorRequest();
    	taxEstimatorRequest.setIncome(new BigDecimal(60000));
    	taxEstimatorRequest.setIncomeIncludesSuperAnnuation(false);
    	taxEstimatorRequest.setTaxRateYearRange(new TaxRateYearRange(new TaxRateYear(2014), new TaxRateYear(2015), new BigDecimal(18783.00)));
    	taxEstimatorRequest.setSuperAnnuationPercentage(9.50);
    	
    }
   
	
	@Test
    public void beanFactoryInitializedCorrectly() {
		assertNotNull("beanFactory should not be null", beanFactory);
    }
	
	@Test
    public void taxCalculatorShouldBeInjected() {
		final TaxCalculator taxCalculator = beanFactory.getBean(TaxCalculator.class, taxEstimatorRequest);
		assertNotNull("taxCalculator should not be null", taxCalculator);
    }
	


}
