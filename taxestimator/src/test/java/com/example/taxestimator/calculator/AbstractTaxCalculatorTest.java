package com.example.taxestimator.calculator;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.taxestimator.models.TaxEstimatorRequest;
import com.example.taxratesmodels.models.TaxRate;
import com.example.taxratesmodels.models.TaxRateYear;
import com.example.taxratesmodels.models.TaxRateYearRange;
import com.example.taxratesmodels.models.TaxRatesPerYear;
import com.example.taxratesmodels.models.TaxableIncome;
import com.example.taxratesmodels.models.TaxableIncomeRange;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
public class AbstractTaxCalculatorTest {
	TaxEstimatorRequest taxEstimatorRequest;
	

    @Before
    public void setup() {
    	
    	taxEstimatorRequest = new TaxEstimatorRequest();
    	taxEstimatorRequest.setIncome(new BigDecimal(60000));
    	taxEstimatorRequest.setIncomeIncludesSuperAnnuation(false);
    	taxEstimatorRequest.setTaxRateYearRange(new TaxRateYearRange(new TaxRateYear(2014), new TaxRateYear(2015), new BigDecimal(18783.00)));
    	taxEstimatorRequest.setSuperAnnuationPercentage(9.50);
    	
    }

	

    @Test
    public void shouldReturnIncome() {
    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateBaseIncome(taxEstimatorRequest.getIncome(), new BigDecimal(1234.56), false);
    	
    	assertEquals(taxEstimatorRequest.getIncome(), amount);
    	
    }
    
    @Test
    public void shouldReturnGrossAmount() {
    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateBaseIncome(taxEstimatorRequest.getIncome(), new BigDecimal(1234.56), true);
    	
    	assertEquals(new BigDecimal(1234.56), amount);
    	
    }
    
    @Test
    public void shouldReturnZero() {
    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateBaseIncome(taxEstimatorRequest.getIncome(), null, true);
    	
    	assertEquals(BigDecimal.ZERO, amount);
    	
    }
    
    
    @Test
    public void shouldCalculateGrossAmount() {    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateGrossAmount(taxEstimatorRequest.getIncome(), new BigDecimal(6500));
    	
    	
    	assertEquals(new BigDecimal(53500.00).setScale(2, RoundingMode.HALF_UP), amount);    	
    }

    
    @Test
    public void shouldCalculateGrossPlusSuperAmount() {    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateGrossPlusSuperAmount(taxEstimatorRequest.getIncome(), new BigDecimal(6500));
    	
    	
    	assertEquals(new BigDecimal(66500.00).setScale(2, RoundingMode.HALF_UP), amount);    	
    }
    
    @Test
    public void shouldCalculateNetIncomeAmount() {    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateNetIncomeAmount(new BigDecimal(5000), taxEstimatorRequest.getIncome());
    	
    	
    	assertEquals(new BigDecimal(55000.00).setScale(2, RoundingMode.HALF_UP), amount);    	
    }
    
    @Test
    public void shouldCalculateNetIncomeAmountReturnZero() {    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateNetIncomeAmount(taxEstimatorRequest.getIncome(), new BigDecimal(5000));
    	
    	
    	assertEquals(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP), amount);    	
    }

    
    @Test
    public void shouldCalculateNetIncomePlusSuperAnnuationAmount() {    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateNetIncomePlusSuperAnnuationAmount(new BigDecimal(5000), taxEstimatorRequest.getIncome());
    	
    	
    	assertEquals(new BigDecimal(65000.00).setScale(2, RoundingMode.HALF_UP), amount);    	
    }
    
    @Test
    public void shouldCalculateNetIncomePlusSuperAnnuationAmountReturnZero() {    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateNetIncomePlusSuperAnnuationAmount(null, taxEstimatorRequest.getIncome());
    	
    	
    	assertEquals(BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP), amount);    	
    }
    
    @Test
    public void shouldCalculateSuperAnnuationAmount() {    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateSuperAnnuationAmount(taxEstimatorRequest.getIncome(), taxEstimatorRequest.getSuperAnnuationPercentage(), false, new BigDecimal(18783.00));
    	
    	
    	assertEquals(new BigDecimal(5700.00).setScale(2, RoundingMode.HALF_UP), amount);    	
    }
    
    @Test
    public void shouldCalculateSuperAnnuationAmount2() {    	
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateSuperAnnuationAmount(taxEstimatorRequest.getIncome(), taxEstimatorRequest.getSuperAnnuationPercentage(), true, new BigDecimal(18783.00));
    	
    	
    	assertEquals(new BigDecimal(5205.48).setScale(2, RoundingMode.HALF_UP), amount);    	
    }

    @Test
    public void shouldCalculateTaxAmount() {   
    	
    	TaxableIncomeRange incomeRange1 = new TaxableIncomeRange(new BigDecimal(0.00), new BigDecimal(18200.00));
		TaxableIncomeRange incomeRange2 = new TaxableIncomeRange(new BigDecimal(18201.00), new BigDecimal(37000.00));
		TaxableIncomeRange incomeRange3 = new TaxableIncomeRange(new BigDecimal(37001.00), new BigDecimal(87000.00));
		TaxableIncomeRange incomeRange4 = new TaxableIncomeRange(new BigDecimal(87001.00), new BigDecimal(180000.00));
		TaxableIncomeRange incomeRange5 = new TaxableIncomeRange(new BigDecimal(180001.00), null);
		
		
    	
    	List<TaxableIncome> taxableIncomes = new ArrayList<>(5);		
		TaxRateYearRange yearRange = new TaxRateYearRange(new TaxRateYear(2017), new TaxRateYear(2018), new BigDecimal(20048.80));
		TaxRate taxRate2 = new TaxRate(yearRange, new BigDecimal(0.00), new BigDecimal(.19));
		TaxRate taxRate3 = new TaxRate(yearRange, new BigDecimal(3572.00), new BigDecimal(.325));
		TaxRate taxRate4 = new TaxRate(yearRange, new BigDecimal(19822.00), new BigDecimal(.37));
		TaxRate taxRate5 = new TaxRate(yearRange, new BigDecimal(54232.00), new BigDecimal(.45));		
		taxableIncomes.add(new TaxableIncome(incomeRange1, null));
		taxableIncomes.add(new TaxableIncome(incomeRange2, taxRate2));
		taxableIncomes.add(new TaxableIncome(incomeRange3, taxRate3));
		taxableIncomes.add(new TaxableIncome(incomeRange4, taxRate4));
		taxableIncomes.add(new TaxableIncome(incomeRange5, taxRate5));

		TaxRatesPerYear taxRatesPerYear = new TaxRatesPerYear(yearRange, taxableIncomes);
		
		
    	TaxCalculatorImpl taxCalulator = new TaxCalculatorImpl(taxEstimatorRequest);
    	BigDecimal amount = taxCalulator.calculateTaxAmount(taxRatesPerYear, taxEstimatorRequest.getIncome());
    	
    	
    	assertEquals(new BigDecimal(11047.00).setScale(2, RoundingMode.HALF_UP), amount);    	
    }
    
    
}

	