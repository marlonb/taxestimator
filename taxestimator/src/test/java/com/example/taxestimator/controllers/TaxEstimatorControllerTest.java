package com.example.taxestimator.controllers;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.example.taxestimator.services.TaxCalculationHistoryService;
import com.example.taxestimator.services.TaxEstimatorService;
import com.example.taxestimator.services.TaxRatesService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TaxEstimatorController.class)
public class TaxEstimatorControllerTest {
	@Autowired
    private TaxEstimatorController controller;
	
	@Autowired
	private MockMvc mockMvc;
	
	
	
	@MockBean
	private TaxRatesService taxRatesService;


	@MockBean
	private TaxEstimatorService taxEstimatorService;

	@MockBean
	private TaxCalculationHistoryService taxCalculationHistoryService;	
	
	
	
	
    @Test
    public void controllerShouldBeInjected() {
    	assertNotNull("controller should not be null", controller);
    }

}
