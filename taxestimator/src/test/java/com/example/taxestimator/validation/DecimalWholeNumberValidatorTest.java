package com.example.taxestimator.validation;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
public class DecimalWholeNumberValidatorTest {
	
	@Test
	public void shouldReturnTrue() {
		DecimalWholeNumberValidator validator = new DecimalWholeNumberValidator();
		assertTrue(validator.isValid(new BigDecimal(123.00), null));
	}
	
	@Test
	public void shouldReturnFalse() {
		DecimalWholeNumberValidator validator = new DecimalWholeNumberValidator();
		assertFalse(validator.isValid(new BigDecimal(123.10), null));
	}
	
	@Test
	public void shouldReturnFalse2() {
		DecimalWholeNumberValidator validator = new DecimalWholeNumberValidator();
		assertFalse(validator.isValid(null, null));
	}

}
