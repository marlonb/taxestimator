package com.example.taxestimator.validation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import javax.validation.ConstraintValidatorContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
public class DoubleMinValidatorTest {
	private DoubleMinValidator validator;
	
	@Before
	public void setup() {
		validator = new DoubleMinValidator() {
			@Override
			public void initialize(DoubleMin constraintAnnotation) {
				this.minValue = 23.54;
			}
			
		};
		validator.initialize(null);
		
	}

	
	@Test
	public void shouldReturnFalse() {
		assertFalse(validator.isValid(12.00, null));
	}
	
	@Test
	public void shouldReturnTrue() {
		assertTrue(validator.isValid(23.54, null));
	}
	
	@Test
	public void shouldReturnTrue2() {
		assertTrue(validator.isValid(123.10, null));
	}
	
	@Test
	public void shouldReturnFalse2() {
		assertFalse(validator.isValid(null, null));
	}
}
