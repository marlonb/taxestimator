package com.example.taxestimator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 * The main Tax Estimator Spring boot app.
 * 
 * @author Marlon.Bautista
 *
 */
@SpringBootApplication
@EnableScheduling
public class TaxEstimatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaxEstimatorApplication.class, args);
	}
}
