package com.example.taxestimator.models;

/**
 * JwtAuthenticationRequest DTO
 * @author Marlon.Bautista
 *
 */
public class JwtAuthenticationRequest {
	private String username;
	private String password;

	public JwtAuthenticationRequest() {
		super();
	}

	
	public JwtAuthenticationRequest(String username) {
		super();
		this.username = username;
	}


	public JwtAuthenticationRequest(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}


	public String getPassword() {
		return this.password;
	}

}
