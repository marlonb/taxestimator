package com.example.taxestimator.models;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.NotNull;

import com.example.taxestimator.validation.DecimalWholeNumber;
import com.example.taxestimator.validation.DoubleMax;
import com.example.taxestimator.validation.DoubleMin;
import com.example.taxratesmodels.models.TaxRateYearRange;

/**
 * TaxEstimatorRequest DTO, using Java Bean Validation API and custom validations
 * 
 * @author Marlon.Bautista
 *
 */
public class TaxEstimatorRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4844542841498067118L;
	
	
	@DoubleMin(message = "Super Annuation Percentage must be greater than or equal to 9.50", minValue = "9.50")
	@DoubleMax(message = "Super Annuation Percentage must be less than or equal to 25.00", maxValue = "25.00")
	private double superAnnuationPercentage;
	
	@NotNull(message = "Income must not be null")
	@DecimalMax(message = "Income must be less than or equal to 99999999999", value = "99999999999")
	@DecimalWholeNumber(message = "Income must be in whole dollars")
	private BigDecimal income;
	
	@NotNull(message = "Tax Rate Year Range must not be null")
	private TaxRateYearRange taxRateYearRange;
	private boolean incomeIncludesSuperAnnuation;
	
	
	public TaxEstimatorRequest() {
		super();
		// TODO Auto-generated constructor stub
	}


	public double getSuperAnnuationPercentage() {
		return superAnnuationPercentage;
	}


	public void setSuperAnnuationPercentage(double superAnnuationPercentage) {
		this.superAnnuationPercentage = superAnnuationPercentage;
	}


	public BigDecimal getIncome() {
		return income;
	}


	public void setIncome(BigDecimal income) {
		this.income = income;
	}


	public TaxRateYearRange getTaxRateYearRange() {
		return taxRateYearRange;
	}


	public void setTaxRateYearRange(TaxRateYearRange taxRateYearRange) {
		this.taxRateYearRange = taxRateYearRange;
	}


	public boolean isIncomeIncludesSuperAnnuation() {
		return incomeIncludesSuperAnnuation;
	}


	public void setIncomeIncludesSuperAnnuation(boolean incomeIncludesSuperAnnuation) {
		this.incomeIncludesSuperAnnuation = incomeIncludesSuperAnnuation;
	}


	
	

}
