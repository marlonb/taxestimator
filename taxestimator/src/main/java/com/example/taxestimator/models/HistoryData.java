package com.example.taxestimator.models;

import java.time.LocalDateTime;


/**
 * HistoryData DTO
 * 
 * @author Marlon.Bautista
 *
 */
public class HistoryData {
	private LocalDateTime timestamp;
	private TaxEstimatorRequest request;
	private TaxEstimatorResponse response;
	
	
	public HistoryData() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public HistoryData(LocalDateTime timestamp, TaxEstimatorRequest request, TaxEstimatorResponse response) {
		super();
		this.timestamp = timestamp;
		this.request = request;
		this.response = response;
		
	}


	public LocalDateTime getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	public TaxEstimatorRequest getRequest() {
		return request;
	}
	public void setRequest(TaxEstimatorRequest request) {
		this.request = request;
	}
	public TaxEstimatorResponse getResponse() {
		return response;
	}
	public void setResponse(TaxEstimatorResponse response) {
		this.response = response;
	}
	
	

}
