package com.example.taxestimator.models;

import java.time.LocalDateTime;

/**
 * TaxEstimatorError DTO
 * 
 * @author Marlon.Bautista
 *
 */
public class TaxEstimatorError {
	private LocalDateTime timestamp;
	private String message;
	private String details;

	public TaxEstimatorError(LocalDateTime timestamp, String message, String details) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.details = details;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public String getDetails() {
		return details;
	}
	
	

}
