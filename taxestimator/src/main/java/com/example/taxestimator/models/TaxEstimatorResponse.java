package com.example.taxestimator.models;

import java.io.Serializable;
import java.math.BigDecimal;

import com.example.taxratesmodels.models.TaxableIncome;

/**
 * The response DTO
 * 
 * @author Marlon.Bautista
 *
 */
public class TaxEstimatorResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2470227232034645329L;
	
	private BigDecimal superAnnuationAmt;
	private BigDecimal grossAmt;
	private BigDecimal grossAmtPlusSuperAmt;
	private BigDecimal taxAmt;
	private BigDecimal netAmt;
	private BigDecimal netAmtPlusSuperAmt;
	private TaxableIncome relatedTaxRate;
	
	
	public TaxEstimatorResponse() {
		super();
		// TODO Auto-generated constructor stub
	}


	public BigDecimal getSuperAnnuationAmt() {
		return superAnnuationAmt;
	}


	public void setSuperAnnuationAmt(BigDecimal superAnnuationAmt) {
		this.superAnnuationAmt = superAnnuationAmt;
	}


	public BigDecimal getGrossAmt() {
		return grossAmt;
	}


	public void setGrossAmt(BigDecimal grossAmt) {
		this.grossAmt = grossAmt;
	}


	public BigDecimal getGrossAmtPlusSuperAmt() {
		return grossAmtPlusSuperAmt;
	}


	public void setGrossAmtPlusSuperAmt(BigDecimal grossAmtPlusSuperAmt) {
		this.grossAmtPlusSuperAmt = grossAmtPlusSuperAmt;
	}


	public BigDecimal getTaxAmt() {
		return taxAmt;
	}


	public void setTaxAmt(BigDecimal taxAmt) {
		this.taxAmt = taxAmt;
	}


	public BigDecimal getNetAmt() {
		return netAmt;
	}


	public void setNetAmt(BigDecimal netAmt) {
		this.netAmt = netAmt;
	}


	public BigDecimal getNetAmtPlusSuperAmt() {
		return netAmtPlusSuperAmt;
	}


	public void setNetAmtPlusSuperAmt(BigDecimal netAmtPlusSuperAmt) {
		this.netAmtPlusSuperAmt = netAmtPlusSuperAmt;
	}


	public TaxableIncome getRelatedTaxRate() {
		return relatedTaxRate;
	}


	public void setRelatedTaxRate(TaxableIncome relatedTaxRate) {
		this.relatedTaxRate = relatedTaxRate;
	}


	
	
}
