package com.example.taxestimator.services;

import java.util.List;

import com.example.taxratesmodels.models.TaxRateYearRange;
import com.example.taxratesmodels.models.TaxRatesPerYear;
/**
 * The Tax rates service
 * @author Marlon.Bautista
 *
 */
public interface TaxRatesService {
	/**
	 * Returns the taxrates.
	 * @return
	 */
	List<TaxRatesPerYear> getTaxRates();
	/**
	 * Returns the tax rates year range list
	 */
	List<TaxRateYearRange> getAllTaxRateYearRanges();
	
	/**
	 * Returns the tax rates for the specified TaxRateYearRange
	 * 
	 * @param taxRateYearRange
	 * @return
	 */
	TaxRatesPerYear getTaxRatesForTaxYearRange(TaxRateYearRange taxRateYearRange);
	

}
