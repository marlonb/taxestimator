package com.example.taxestimator.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.taxratesmodels.models.TaxRateYearRange;
import com.example.taxratesmodels.models.TaxRatesPerYear;


/**
 * The default TaxRatesService implementation class.
 * 
 * @author Marlon.Bautista
 *
 */
@Service
public class TaxRatesServiceImpl implements TaxRatesService {
	@Autowired
	private TaxRatesCacheService taxRatesCacheService;
	
	
	@Override
	public List<TaxRatesPerYear> getTaxRates() {
		return this.taxRatesCacheService.getTaxRatesCache();
	}
	
	@Override
	public List<TaxRateYearRange> getAllTaxRateYearRanges() {
		return this.getTaxRates().stream().map(taxRate -> taxRate.getTaxRateYearRange()).collect(Collectors.toList());
	}


	@Override
	public TaxRatesPerYear getTaxRatesForTaxYearRange(TaxRateYearRange taxRateYearRange) {
		return this.getTaxRates().stream().filter(taxRate -> taxRate.getTaxRateYearRange().equals(taxRateYearRange)).findAny().orElse(null);
	
	}






}
