package com.example.taxestimator.services;

import com.example.taxestimator.models.TaxEstimatorRequest;
import com.example.taxestimator.models.TaxEstimatorResponse;
/**
 * Tax Estimator service.
 * 
 * @author Marlon.Bautista
 *
 */
public interface TaxEstimatorService {
	
	/**
	 * Given a request, the applicable calculations are performed.
	 * 
	 * @param request The tax calculation request 
	 * @param username The user associated with the request.
	 * 
	 * @return Calculated values (TaxEstimatorResponse)
	 */
	TaxEstimatorResponse calculateTax(TaxEstimatorRequest request, String username);	

}
