package com.example.taxestimator.services;

import java.util.List;

import com.example.taxestimator.domain.TaxCalculationHistory;
import com.example.taxestimator.models.HistoryData;

public interface TaxCalculationHistoryService {
	/**
	 * Queries all calculation history for the given username.
	 * 
	 * @param username The username to query with.
	 * @return Returns a list of all the tax calculation history for the given user.
	 * 
	 */
	List<TaxCalculationHistory> findHistoryForUsername(String username);	
	//boolean existsByUsername(String username);
	
	/**
	 * Deletes all the tax calculation history for the given user.
	 * 
	 * @param username The user to delete all transactions for.
	 * @return the number of deleted records.
	 */
	long deleteByUsername(String username);
	
	/**
	 * Inserts the history data for the given user.
	 * 
	 * @param username The user
	 * @param historyData The Tax Calculation History data to insert
	 */
	void save(String username, HistoryData historyData);
}
