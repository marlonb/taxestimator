package com.example.taxestimator.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.taxestimator.domain.TaxCalculationHistory;
import com.example.taxestimator.models.HistoryData;
import com.example.taxestimator.repositories.TransactionHistoryRepository;


/**
 * The default TaxCalculationHistoryService implementation.
 * 
 * @author Marlon.Bautista
 *
 */
@Service
public class TaxCalculationHistoryServiceImpl implements TaxCalculationHistoryService {
	@Autowired
	private TransactionHistoryRepository repository;


	@Override
	public List<TaxCalculationHistory> findHistoryForUsername(String username) {
		return repository.findByUsernameOrderByTimestampDesc(username);
	}
	
	@Override
	public long deleteByUsername(String username) {
		return this.repository.deleteByUsername(username);
		
	}
	//@Override
	//public boolean existsByUsername(String username) {
	//	return repository.existsByUsername(username);
	//}

	@Override
	public void save(String username, HistoryData historyData) {
		TaxCalculationHistory taxCalculationHistory = new TaxCalculationHistory(username, 
				historyData.getTimestamp(), 
				historyData.getRequest(), 
				historyData.getResponse());
		
		repository.save(taxCalculationHistory);		
	}

}
