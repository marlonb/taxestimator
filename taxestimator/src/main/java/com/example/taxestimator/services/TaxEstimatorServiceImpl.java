package com.example.taxestimator.services;

import java.time.LocalDateTime;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.taxestimator.calculator.TaxCalculator;
import com.example.taxestimator.models.HistoryData;
import com.example.taxestimator.models.TaxEstimatorRequest;
import com.example.taxestimator.models.TaxEstimatorResponse;


/**
 *  The default TaxEstimatorService implementation.
 *  
 * @author Marlon.Bautista
 *
 */
@Service
public class TaxEstimatorServiceImpl implements TaxEstimatorService {
	@Autowired
	private BeanFactory beanFactory;
	@Autowired
	private TaxCalculationHistoryService taxCalculationHistoryService;

	@Override
	@Transactional
	/**
	 * Performs applicable tax calculations then save the transaction history to the Db.
	 * 
	 */
	public TaxEstimatorResponse calculateTax(TaxEstimatorRequest request, String username) {
		final TaxCalculator taxCalculator = beanFactory.getBean(TaxCalculator.class, request);
		final TaxEstimatorResponse response = taxCalculator.calculateTax();
		
		createOrUpdateTransactionHistory(request, response, username);
		
		return response;
	}

	private void createOrUpdateTransactionHistory(TaxEstimatorRequest request, TaxEstimatorResponse response, String username) {
		final HistoryData historyData = new HistoryData(LocalDateTime.now(), request, response);		
		taxCalculationHistoryService.save(username, historyData);
	}	

}
