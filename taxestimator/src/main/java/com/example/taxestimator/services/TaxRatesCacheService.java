package com.example.taxestimator.services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.example.taxestimator.constants.TaxEstimatorConstant;
import com.example.taxratesmodels.models.TaxRatesPerYear;


/**
 * The tax rates cache. 
 * The cache is updated every 5 minutes by calling an external tax rates provider service.
 * 
 * 
 * 
 * @author Marlon.Bautista
 *
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class TaxRatesCacheService {
	private static final Logger LOGGER = LoggerFactory.getLogger(TaxRatesCacheService.class);
	
	@Value("${" + TaxEstimatorConstant.TAX_RATES_RESOURCE_HOST_PROP_NAME + "}")
	private String taxRatesResourceHost;

	@Value("${" + TaxEstimatorConstant.TAX_RATES_RESOURCE_PORT_PROP_NAME + "}")
	private String taxRatesResourcePort;	

	private List<TaxRatesPerYear> taxRatesCache;

	@PostConstruct
	public void init() {
		LOGGER.debug("in TaxRatesCacheService.init()");
		this.updateTaxRatesCache();
	}
	
	

	public List<TaxRatesPerYear> getTaxRatesCache() {
		return this.taxRatesCache;
	}

	@Scheduled(fixedRate = 300000)
	public void updateTaxRatesCache() {		
		try {
			final RestTemplate restTemplate = new RestTemplate();
			//final String hostInfo = this.taxRatesResourceHost + TaxEstimatorConstant.TAX_RATES_RESOURCE_API_ENDPOINT;
			final String hostInfo = "http://" + this.taxRatesResourceHost + ":" + this.taxRatesResourcePort + TaxEstimatorConstant.TAX_RATES_RESOURCE_API_ENDPOINT;

			final ResponseEntity<List<TaxRatesPerYear>> rateResponse =
					restTemplate.exchange(hostInfo,
							HttpMethod.GET, null, new ParameterizedTypeReference<List<TaxRatesPerYear>>() {
					});
			final List<TaxRatesPerYear> taxRatesList = rateResponse.getBody();
			
			LOGGER.debug("Updating taxRatesCache");
			this.taxRatesCache = taxRatesList;
		} catch (Exception e) {
			LOGGER.error("Exception in updateTaxRatesCache(): " + e.getMessage());
			this.taxRatesCache = new ArrayList<>();
		}		
	}

}
