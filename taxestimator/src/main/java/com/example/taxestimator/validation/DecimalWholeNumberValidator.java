package com.example.taxestimator.validation;

import java.math.BigDecimal;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.stereotype.Component;

/**
 * DecimalWholeNumber validation implementation
 * @author Marlon.Bautista
 *
 */
@Component
public class DecimalWholeNumberValidator implements ConstraintValidator<DecimalWholeNumber, BigDecimal> {
	
	@Override
	public boolean isValid(BigDecimal value, ConstraintValidatorContext context) {
		return value != null && (value.scale() <= 0 || value.remainder(BigDecimal.ONE).compareTo(BigDecimal.ZERO) == 0);
	}	
}
