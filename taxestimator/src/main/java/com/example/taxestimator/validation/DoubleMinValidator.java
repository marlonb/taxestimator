package com.example.taxestimator.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.stereotype.Component;

/**
 * Default DoubleMin validation implementation
 * @author Marlon.Bautista
 *
 */
@Component
public class DoubleMinValidator implements ConstraintValidator<DoubleMin, Double> {
	protected Double minValue; 

	@Override
	public void initialize(DoubleMin constraintAnnotation) {
		minValue = Double.valueOf(constraintAnnotation.minValue());
	}
	
	
	@Override
	public boolean isValid(Double value, ConstraintValidatorContext paramConstraintValidatorContext) {
		// ODO Auto-generated method stub
		return (value != null && value.compareTo(minValue) >= 0);
	}


	
}
