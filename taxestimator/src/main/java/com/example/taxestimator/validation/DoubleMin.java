package com.example.taxestimator.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.*;
import javax.validation.Constraint;
import javax.validation.Payload;
/**
 * Validates if the double property is greater than or equal to the minValue().
 * @author Marlon.Bautista
 *
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { DoubleMinValidator.class })
public @interface DoubleMin {
	String message() default "Property must be greater than or equal to value";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
	
	String minValue();
}
