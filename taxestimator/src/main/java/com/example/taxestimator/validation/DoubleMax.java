package com.example.taxestimator.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.*;
import javax.validation.Constraint;
import javax.validation.Payload;
/**
 * Validates if the double property is less than or equal to the maxValue()
 * 
 * @author Marlon.Bautista
 *
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { DoubleMaxValidator.class })
public @interface DoubleMax {
	String message() default "Property must be less than or equal to value";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
	
	String maxValue();
}
