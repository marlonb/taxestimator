package com.example.taxestimator.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.stereotype.Component;
/**
 * DoubleMax validation implementation.
 * 
 * @author Marlon.Bautista
 *
 */
@Component
public class DoubleMaxValidator implements ConstraintValidator<DoubleMax, Double> {
	protected Double maxValue; 

	@Override
	public void initialize(DoubleMax constraintAnnotation) {
		maxValue = Double.valueOf(constraintAnnotation.maxValue());
	}
	
	
	@Override
	public boolean isValid(Double value, ConstraintValidatorContext paramConstraintValidatorContext) {
		// ODO Auto-generated method stub
		return (value != null && value.compareTo(maxValue) <= 0);
	}


	
}
