package com.example.taxestimator.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.annotation.*;
import javax.validation.Constraint;
import javax.validation.Payload;
/**
 * Validates if the given amount is in whole dollars.
 * 
 * @author Marlon.Bautista
 *
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { DecimalWholeNumberValidator.class })
public @interface DecimalWholeNumber {
	String message() default "Amount must be in whole dollars";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
	
}
