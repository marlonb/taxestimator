package com.example.taxestimator.domain;

import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.example.taxestimator.models.TaxEstimatorRequest;
import com.example.taxestimator.models.TaxEstimatorResponse;


/**
 * TaxCalculationHistory entity
 * 
 * @author Marlon.Bautista
 *
 */

@Document(collection = "taxCalculationHistories")
@CompoundIndexes({
	@CompoundIndex(name = "username_timestamp", def = "{'username' : 1, 'timestamp': 1}")
})
public class TaxCalculationHistory {
	@Id
	private String id;
	private String username;    
	private LocalDateTime timestamp;
	private TaxEstimatorRequest request;
	private TaxEstimatorResponse response;


	public TaxCalculationHistory(String username, LocalDateTime timestamp, TaxEstimatorRequest request, TaxEstimatorResponse response) {	
		super();
		this.username = username;
		this.timestamp = timestamp;
		this.request = request;
		this.response = response;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public LocalDateTime getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}


	public TaxEstimatorRequest getRequest() {
		return request;
	}


	public void setRequest(TaxEstimatorRequest request) {
		this.request = request;
	}


	public TaxEstimatorResponse getResponse() {
		return response;
	}


	public void setResponse(TaxEstimatorResponse response) {
		this.response = response;
	}



	



}
