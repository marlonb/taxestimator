package com.example.taxestimator.calculator;

import com.example.taxestimator.models.TaxEstimatorRequest;

/**
 * Base TaxCalculator implementation.
 * Delegates the flow to AbstractTaxCalculator.
 * 
 * @author Marlon.Bautista
 *
 */
public class TaxCalculatorImpl extends AbstractTaxCalculator implements TaxCalculator {
	public TaxCalculatorImpl(TaxEstimatorRequest taxEstimatorRequest) {
		super(taxEstimatorRequest);
	}


}
