package com.example.taxestimator.calculator;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.example.taxestimator.constants.TaxEstimatorConstant;
import com.example.taxestimator.exception.TaxEstimatorException;
import com.example.taxestimator.models.TaxEstimatorRequest;
import com.example.taxestimator.models.TaxEstimatorResponse;
import com.example.taxestimator.operators.GrossAmountOperator;
import com.example.taxestimator.operators.GrossPlusSuperAmountOperator;
import com.example.taxestimator.operators.IncomeTaxOperator;
import com.example.taxestimator.operators.NetAmountOperator;
import com.example.taxestimator.operators.NetIncomePlusSuperAmountOperator;
import com.example.taxestimator.operators.SuperAnnuationOperator;
import com.example.taxestimator.operators.TaxAmountOperator;
import com.example.taxestimator.services.TaxRatesService;
import com.example.taxratesmodels.models.TaxRateYearRange;
import com.example.taxratesmodels.models.TaxRatesPerYear;
import com.example.taxratesmodels.models.TaxableIncome;

/**
 * This is the abstract implementation that calls the different calculations performed on TaxEstimatorRequest. 
 * 
 * 
 * @author Marlon.Bautista
 *
 */
public abstract class AbstractTaxCalculator implements TaxCalculator {
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTaxCalculator.class);

	@Autowired
	private TaxRatesService taxRatesService;


	private TaxEstimatorRequest taxEstimatorRequest;
	private TaxEstimatorResponse taxEstimatorResponse;


	/**
	 * 
	 * @param taxEstimatorRequest The request to be calculated.
	 */
	public AbstractTaxCalculator(TaxEstimatorRequest taxEstimatorRequest) {
		this.taxEstimatorRequest = taxEstimatorRequest;
		this.taxEstimatorResponse = new TaxEstimatorResponse();
	}

	@Override
	public TaxEstimatorResponse calculateTax() {		
		final TaxRateYearRange taxRateYearRange = this.taxEstimatorRequest.getTaxRateYearRange();
		if (taxRateYearRange != null) {
			final TaxRatesPerYear taxRatesPerYear = getTaxRatesPerYearForTaxRange(taxRateYearRange);
			if (taxRatesPerYear != null) {				
				final BigDecimal income = this.taxEstimatorRequest.getIncome();
				final double superPercentage = this.taxEstimatorRequest.getSuperAnnuationPercentage();
				final boolean includesSuper = this.taxEstimatorRequest.isIncomeIncludesSuperAnnuation();
				
				return calculate(taxRateYearRange, taxRatesPerYear, income, superPercentage, includesSuper);
			}
			LOGGER.debug("taxRatesPerYear: " + taxRatesPerYear);
			throw new TaxEstimatorException("Tax Rate for the year information can not be null.");						
		} else {
			LOGGER.debug("Tax Rate Year Range is null.");
		}
		return this.taxEstimatorResponse;
	}


	protected TaxEstimatorResponse calculate(TaxRateYearRange taxRateYearRange, TaxRatesPerYear taxRatesPerYear, final BigDecimal income, final double superPercentage, final boolean includesSuper) {		
		if (income.compareTo(BigDecimal.ZERO) > 0) {
			if (validSuperAnnuationPercentage(superPercentage)) {
				//1
				final BigDecimal superAnnuationAmount = calculateSuperAnnuationAmount(income, superPercentage, includesSuper, taxRateYearRange.getMaxSuperContributionAmount());
				
				//2 & 3
				BigDecimal grossAmount = null;
				BigDecimal grossPlusSuperAmount = null;
				if (includesSuper) {
					grossAmount = calculateGrossAmount(income, superAnnuationAmount);
				} else {					
					grossPlusSuperAmount = calculateGrossPlusSuperAmount(income, superAnnuationAmount);
				}
				
				final BigDecimal baseIncome = calculateBaseIncome(income, grossAmount, includesSuper);							
				//4
				final BigDecimal incomeTaxAmount = calculateTaxAmount(taxRatesPerYear, baseIncome);
				//5
				final BigDecimal netIncomeAmount = calculateNetIncomeAmount(incomeTaxAmount, baseIncome);
				//6
				final BigDecimal netIncomePlusSuperAnnuationAmount = calculateNetIncomePlusSuperAnnuationAmount(netIncomeAmount, superAnnuationAmount);
				//for tax calculation history
				final TaxableIncome taxableIncome = getTaxableIncome(taxRatesPerYear, baseIncome);
				
				this.taxEstimatorResponse.setSuperAnnuationAmt(superAnnuationAmount);
				this.taxEstimatorResponse.setGrossAmt(grossAmount);
				this.taxEstimatorResponse.setGrossAmtPlusSuperAmt(grossPlusSuperAmount);
				this.taxEstimatorResponse.setTaxAmt(incomeTaxAmount);
				this.taxEstimatorResponse.setNetAmt(netIncomeAmount);
				this.taxEstimatorResponse.setNetAmtPlusSuperAmt(netIncomePlusSuperAnnuationAmount);
				this.taxEstimatorResponse.setRelatedTaxRate(taxableIncome);
				
			} else {
				//LOG error
			}			
		} else {
			//LOG error
		}


		return this.taxEstimatorResponse;
	}



	protected BigDecimal calculateNetIncomePlusSuperAnnuationAmount(final BigDecimal netAmount, final BigDecimal superAnnuationAmount) {
		final IncomeTaxOperator netIncomePlusSuperOperator = new NetIncomePlusSuperAmountOperator(superAnnuationAmount);
		final BigDecimal netIncomePlusSuperAnnuationAmount = netIncomePlusSuperOperator.calculate(netAmount);			
		
		return netIncomePlusSuperAnnuationAmount;		
	}

	protected BigDecimal calculateNetIncomeAmount(final BigDecimal incomeTaxAmount, final BigDecimal baseIncome) {
		final IncomeTaxOperator netAmountOperator = new NetAmountOperator(incomeTaxAmount);
		final BigDecimal netIncomeAmount = netAmountOperator.calculate(baseIncome);
		return netIncomeAmount;
		
	}

	protected BigDecimal calculateTaxAmount(TaxRatesPerYear taxRatesPerYear, final BigDecimal baseIncome) {
		final IncomeTaxOperator taxAmtOperator = new TaxAmountOperator(taxRatesPerYear);	
		final BigDecimal incomeTaxAmount = taxAmtOperator.calculate(baseIncome);
		
		return incomeTaxAmount;
	}
	
	protected TaxableIncome getTaxableIncome(TaxRatesPerYear taxRatesPerYear, BigDecimal baseIncome) {
		final TaxAmountOperator taxAmtOperator = new TaxAmountOperator(taxRatesPerYear);
		
		final TaxableIncome taxableIncome = taxAmtOperator.getTaxableIncome(baseIncome);
		if (taxableIncome == null) {
			//log			
		}		
		return taxableIncome;
	}

	protected BigDecimal calculateBaseIncome(final BigDecimal income, final BigDecimal grossAmount, final boolean includesSuper) {
		BigDecimal baseIncome = BigDecimal.ZERO;
		if (includesSuper) {
			//gross amount is the base income
			if (grossAmount != null) {
				baseIncome = grossAmount;	
			} else {
				//log
			}			
		} else {					
			baseIncome = income;
		}
		return baseIncome;
	}

	protected BigDecimal calculateGrossPlusSuperAmount(final BigDecimal income, final BigDecimal superAnnuationAmount) {
		final IncomeTaxOperator grossPlusSuperAmountOperator = new GrossPlusSuperAmountOperator(superAnnuationAmount);
		final BigDecimal grossPlusSuperAmt = grossPlusSuperAmountOperator.calculate(income);


		return grossPlusSuperAmt;
	}

	protected BigDecimal calculateGrossAmount(final BigDecimal income, final BigDecimal superAnnuationAmount) {
		final IncomeTaxOperator grossAmountOperator = new GrossAmountOperator(superAnnuationAmount);
		final BigDecimal grossAmt = grossAmountOperator.calculate(income);

		return grossAmt;
	}

	protected BigDecimal calculateSuperAnnuationAmount(final BigDecimal income, final double superPercentage, final boolean includesSuper, BigDecimal maxSuperContributionAmount) {
		final IncomeTaxOperator superAnnuationOperator = new SuperAnnuationOperator(superPercentage, includesSuper, maxSuperContributionAmount);
		final BigDecimal superAnnuationAmount = superAnnuationOperator.calculate(income);
		
		return superAnnuationAmount;		
	}

	private boolean validSuperAnnuationPercentage(final double superAnnuationPercentage) {
		return Double.valueOf(superAnnuationPercentage).compareTo(TaxEstimatorConstant.SUPER_ANNUATION_MIN_PERCENTAGE) >= 0
				&& Double.valueOf(superAnnuationPercentage).compareTo(TaxEstimatorConstant.SUPER_ANNUATION_MAX_PERCENTAGE) <= 0;

	}

	protected TaxRatesPerYear getTaxRatesPerYearForTaxRange(TaxRateYearRange taxRateYearRange) {

		return taxRatesService.getTaxRatesForTaxYearRange(taxRateYearRange);
	}



}
