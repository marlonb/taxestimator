package com.example.taxestimator.calculator;

import com.example.taxestimator.models.TaxEstimatorResponse;

/**
 * 
 * @author Marlon.Bautista
 *
 */
public interface TaxCalculator {
	/**
	 * Returns a TaxEstimatorResponse after all the operators are called.
	 * 
	 * @return
	 */
	TaxEstimatorResponse calculateTax();
}
