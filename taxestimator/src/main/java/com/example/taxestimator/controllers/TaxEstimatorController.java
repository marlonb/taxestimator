package com.example.taxestimator.controllers;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.taxestimator.constants.TaxEstimatorConstant;
import com.example.taxestimator.domain.TaxCalculationHistory;
import com.example.taxestimator.models.TaxEstimatorRequest;
import com.example.taxestimator.models.TaxEstimatorResponse;
import com.example.taxestimator.services.TaxCalculationHistoryService;
import com.example.taxestimator.services.TaxEstimatorService;
import com.example.taxestimator.services.TaxRatesService;
//import com.example.taxestimator.util.JwtTokenUtil;
import com.example.taxratesmodels.models.TaxRateYearRange;
import com.example.taxratesmodels.models.TaxRatesPerYear;


/**
 * TaxEstimator REST endpoints
 * 
 * @author Marlon.Bautista
 *
 */

@RestController
@RequestMapping(TaxEstimatorConstant.TAX_ESTIMATOR_APP_API_ENDPOINT)
public class TaxEstimatorController {
	@Autowired
	private TaxRatesService taxRatesService;


	@Autowired
	private TaxEstimatorService taxEstimatorService;

	@Autowired
	private TaxCalculationHistoryService taxCalculationHistoryService;	

	//@GetMapping(value = "/{path:[^\\.]*}")
	//@GetMapping("/{path:[^\\.]+}/**")
	@RequestMapping(value = "/**/{[path:[^\\.]*}")      
	public String redirect() {
		return "forward:/";
	}



	@GetMapping("/taxrates")
	public List<TaxRatesPerYear> getTaxRates() {
		return taxRatesService.getTaxRates();
	}

	@GetMapping("/taxrates/yearranges")
	public List<TaxRateYearRange> getAllTaxRateYearRanges() {
		return taxRatesService.getAllTaxRateYearRanges();
	}

	@GetMapping("/history")
	public List<TaxCalculationHistory> getHistoryList(Principal principal) {
		return taxCalculationHistoryService.findHistoryForUsername(principal.getName());
	}

	@DeleteMapping("/history")
	public ResponseEntity<?> deleteHistories(Principal principal) {
		final long deletedCount = taxCalculationHistoryService.deleteByUsername(principal.getName());
		return ResponseEntity.ok().build();
	}

	@PostMapping("/calculate")
	public TaxEstimatorResponse calculate(@Valid @RequestBody TaxEstimatorRequest request, Principal principal) {
		return taxEstimatorService.calculateTax(request, principal.getName());
	}







}
