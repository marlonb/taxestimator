package com.example.taxestimator.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.taxestimator.constants.SecurityConstant;


/**
 * The spring web security configuration
 * 
 * @author Marlon.Bautista
 *
 */

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Autowired
	private UserDetailsService userDetailsService;

	

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
		.userDetailsService(this.userDetailsService)
		.passwordEncoder(this.bCryptPasswordEncoder);
	}



	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http
        .sessionManagement()
        //Don't create session. The Authorization header is always validated.
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        //csrf not needed. if you don't have the jwt you can't do anything
		.and().csrf().disable()
		//start authorizing requests
		.authorizeRequests()
		//permit login endpoint
		.antMatchers(HttpMethod.POST, SecurityConstant.CUSTOM_LOGIN_URL).permitAll()
		//permit js and html
		.antMatchers(HttpMethod.GET, "/",
				"/*.html",
				"/favicon.ico",
				"/**/*.html",
				"/**/*.css",
				"/**/*.js").permitAll()
		//other requests are authenticated
		.anyRequest().authenticated()
		.and()
		//add the filters
		.addFilter(new JWTAuthenticationFilter(authenticationManager()))
		.addFilter(new JWTAuthorizationFilter(authenticationManager()));
	}

}
