package com.example.taxestimator.config;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.example.taxestimator.constants.SecurityConstant;
import com.example.taxestimator.models.JwtAuthenticationRequest;
import com.example.taxestimator.userdetails.TaxEstimatorUserDetails;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * The JWT Filter class that performs the authentication process
 * of the credentials passed.
 * 
 * @author Marlon.Bautista
 *
 */
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private static final Logger LOGGER = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

	private AuthenticationManager authenticationManager;
	
	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
		 setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher(SecurityConstant.CUSTOM_LOGIN_URL,
				 HttpMethod.POST.toString()));
	}
	@Override
	public Authentication attemptAuthentication(HttpServletRequest req,
			HttpServletResponse res) throws AuthenticationException {
		try {
			JwtAuthenticationRequest credentials = new ObjectMapper()
					.readValue(req.getInputStream(), JwtAuthenticationRequest.class);
			return authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(
							credentials.getUsername(),
							credentials.getPassword(),
							new ArrayList<>())
					);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	@Override
	/**
	 * On successful authentication, the generated token is added to the header.
	 */
	protected void successfulAuthentication(HttpServletRequest req,
			HttpServletResponse res,
			FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		final String username = ((TaxEstimatorUserDetails) auth.getPrincipal()).getUsername();
		String token = Jwts.builder()
				.setSubject(username)
				//.setExpiration(new Date(System.currentTimeMillis() + SecurityConstant.EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SecurityConstant.SECRET_KEY)
				.compact();

		res.addHeader(SecurityConstant.AUTH_HEADER_KEY, SecurityConstant.TOKEN_PREFIX + token);
		

        try {
        	JwtAuthenticationRequest credential = new JwtAuthenticationRequest(username);
            String jwtResponse = new ObjectMapper().writeValueAsString(credential);
            res.setContentType("application/json");
            //res.getWriter().write(user.toString());
            res.getWriter().write(jwtResponse);
        } catch (Exception e) {
        	LOGGER.error(e.getMessage());
            System.out.println(e.getMessage());
        }
	}
	
	
}