package com.example.taxestimator.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

import com.example.taxestimator.calculator.TaxCalculator;
import com.example.taxestimator.calculator.TaxCalculatorImpl;
import com.example.taxestimator.models.TaxEstimatorRequest;


/**
 * Application service providers.
 * 
 * @author Marlon.Bautista
 *
 */
@Configuration
public class ServiceProvider {
	
	
	@Bean
	@Scope(value = "prototype")
	@Lazy(value = true)
	public TaxCalculator taxCalculator(TaxEstimatorRequest taxEstimatorRequest) {
		return new TaxCalculatorImpl(taxEstimatorRequest);
	}


}
