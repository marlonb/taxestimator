package com.example.taxestimator.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.example.taxestimator.domain.User;
import com.example.taxestimator.repositories.UserRepository;


/**
 * Loads predefined users to the Db.
 * 
 * @author Marlon.Bautista
 *
 */
@Component
public class UsersDataLoader implements CommandLineRunner {
	private static final Logger LOGGER = LoggerFactory.getLogger(UsersDataLoader.class);

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private UserRepository repository;
	
	
	@Override
	public void run(String... args) throws Exception {
		long dataCount = repository.count();
		
		if (dataCount == 0) {
			LOGGER.info("Loding Users Data...");
			
			// save users
			repository.save(new User("user1", bCryptPasswordEncoder.encode("user1")));
			repository.save(new User("user2", bCryptPasswordEncoder.encode("user2")));
			repository.save(new User("user3", bCryptPasswordEncoder.encode("user3")));
			repository.save(new User("user4", bCryptPasswordEncoder.encode("user4")));
			repository.save(new User("user5", bCryptPasswordEncoder.encode("user5")));
			repository.save(new User("user6", bCryptPasswordEncoder.encode("user6")));			
			
		}
		// TODO Auto-generated method stub		
	}
}
