package com.example.taxestimator.operators;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Gross plus super amount calculator class.
 * 
 * @author Marlon.Bautista
 *
 */
public class GrossPlusSuperAmountOperator implements IncomeTaxOperator {
	private BigDecimal superAnnuationAmount;
	public GrossPlusSuperAmountOperator(BigDecimal superAnnuationAmount) {
		super();
		this.superAnnuationAmount = superAnnuationAmount;
	}


	@Override
	public BigDecimal calculate(BigDecimal amount) {
		BigDecimal grossPluSuperAmt = BigDecimal.ZERO;
		if (amount != null) {
			grossPluSuperAmt = amount.add(this.superAnnuationAmount);
		}
		return grossPluSuperAmt.setScale(2, RoundingMode.HALF_UP);		
	}
	
	



}
