package com.example.taxestimator.operators;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.example.taxestimator.constants.TaxEstimatorConstant;

/**
 * Super annuation amount calculator class
 * 
 * @author Marlon.Bautista
 *
 */
public class SuperAnnuationOperator implements IncomeTaxOperator {
	private double superAnnuationPercentage;
	private boolean superAnnuationIncludedInBaseAmount;
	private BigDecimal maxSuperContributionAmount;

	public SuperAnnuationOperator(double superAnnuationPercentage, boolean superAnnuationIncludedInBaseAmount, BigDecimal maxSuperContributionAmount) {
		super();
		this.superAnnuationPercentage = superAnnuationPercentage;
		this.superAnnuationIncludedInBaseAmount = superAnnuationIncludedInBaseAmount;
		this.maxSuperContributionAmount = maxSuperContributionAmount;
	}


	@Override
	public BigDecimal calculate(BigDecimal amount) {
		BigDecimal superAmount = BigDecimal.ZERO;
		if (amount != null && amount.compareTo(TaxEstimatorConstant.BASE_INCOME_FOR_SUPER_ELLIGIBILITY) >= 0) {
			if (this.superAnnuationIncludedInBaseAmount) {
				BigDecimal divisor = BigDecimal.ONE.add(new BigDecimal(this.superAnnuationPercentage / 100));				
				superAmount = amount.subtract(amount.divide(divisor, 2, RoundingMode.HALF_UP));
			} else {
				superAmount = amount.multiply(new BigDecimal(this.superAnnuationPercentage / 100));
			}
		}
		if (superAmount.compareTo(this.maxSuperContributionAmount) >= 0 ) {
			superAmount = this.maxSuperContributionAmount;	
		}
		return superAmount.setScale(2, RoundingMode.HALF_UP);
		
	}
	
	



}
