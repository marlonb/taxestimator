package com.example.taxestimator.operators;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Net income plus super amount calculator class.
 * 
 * @author Marlon.Bautista
 *
 */
public class NetIncomePlusSuperAmountOperator implements IncomeTaxOperator {
	private BigDecimal superAnnuationAmount;

	public NetIncomePlusSuperAmountOperator(BigDecimal superAnnuationAmount) {
		super();
		this.superAnnuationAmount = superAnnuationAmount;
	}


	@Override
	public BigDecimal calculate(BigDecimal amount) {
		BigDecimal netIncomePlusSuperAnnuationAmount = BigDecimal.ZERO;
		if (amount != null && this.superAnnuationAmount != null) {
			netIncomePlusSuperAnnuationAmount = amount.add(this.superAnnuationAmount);
		} else {
			//log
		}
		return netIncomePlusSuperAnnuationAmount.setScale(2, RoundingMode.HALF_UP);				
	}

}
