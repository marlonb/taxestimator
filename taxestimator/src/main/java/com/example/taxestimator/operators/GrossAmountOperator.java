package com.example.taxestimator.operators;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Gross amount calculator class.
 * 
 * @author Marlon.Bautista
 *
 */
public class GrossAmountOperator implements IncomeTaxOperator {
	private BigDecimal superAnnuationAmount;
	public GrossAmountOperator(BigDecimal superAnnuationAmount) {
		super();
		this.superAnnuationAmount = superAnnuationAmount;
	}


	@Override
	public BigDecimal calculate(BigDecimal amount) {
		BigDecimal grossAmt = BigDecimal.ZERO;
		if (amount != null && amount.compareTo(this.superAnnuationAmount) >= 0) {				
			grossAmt = amount.subtract(this.superAnnuationAmount);
		}
		return grossAmt.setScale(2, RoundingMode.HALF_UP);		
	}

}
