package com.example.taxestimator.operators;

import java.math.BigDecimal;
/**
 * Tax calculators interface
 * 
 * @author Marlon.Bautista
 *
 */
public interface IncomeTaxOperator {

	BigDecimal calculate(BigDecimal amount);
}
