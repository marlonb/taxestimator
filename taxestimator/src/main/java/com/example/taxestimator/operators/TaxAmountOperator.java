package com.example.taxestimator.operators;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.example.taxratesmodels.models.TaxRate;
import com.example.taxratesmodels.models.TaxRatesPerYear;
import com.example.taxratesmodels.models.TaxableIncome;

/**
 * Tax amount calculator class.
 * 
 * @author Marlon.Bautista
 *
 */
public class TaxAmountOperator implements IncomeTaxOperator {
	private TaxRatesPerYear taxRatesPerYear;
	
	public TaxAmountOperator(TaxRatesPerYear taxRatesPerYear) {
		super();
		this.taxRatesPerYear = taxRatesPerYear;
	}
	@Override
	public BigDecimal calculate(BigDecimal amount) {
		BigDecimal incomeTaxAmount = BigDecimal.ZERO;
		final TaxableIncome taxableIncome = getTaxableIncome(amount);
		if (taxableIncome != null) {
			incomeTaxAmount = calculateTaxAmount(amount, taxableIncome);			
		
		} else {
			//log
		}
			
		return incomeTaxAmount.setScale(2, RoundingMode.HALF_UP);
	}
	protected BigDecimal calculateTaxAmount(BigDecimal amount, TaxableIncome taxableIncome) {
		BigDecimal incomeTaxAmount = BigDecimal.ZERO;

		TaxRate taxRate = taxableIncome.getTaxRate();
		if (taxRate != null) {
			BigDecimal additionalTaxAmount = calculateAdditionalTaxAmount(amount, taxableIncome, taxRate);
			if (additionalTaxAmount.compareTo(BigDecimal.ZERO) > 0) {
				//add additional tax to incomeTaxAmount
				incomeTaxAmount = additionalTaxAmount;
			}			
			if (taxableIncome.getTaxRate().getBaseTaxAmt() != null) {
				//add base tax plus incomeTaxAmount
				incomeTaxAmount = incomeTaxAmount.add(taxableIncome.getTaxRate().getBaseTaxAmt());
			}
		} else {
			//no tax!			
		}
		return incomeTaxAmount;
	}
	
	
	
	protected BigDecimal calculateAdditionalTaxAmount(BigDecimal amount, TaxableIncome taxableIncome, TaxRate taxRate) {
		BigDecimal additionalTaxAmount = BigDecimal.ZERO;
		if (taxRate.getAdditionalTaxAmt() != null) {
			final BigDecimal wholeDollarsOver = getWholeDollarsOver(amount, taxableIncome, taxRate);
			if (wholeDollarsOver != null) {
				additionalTaxAmount = wholeDollarsOver.multiply(taxRate.getAdditionalTaxAmt());
			}
		}
		return additionalTaxAmount;
		
		
	}
	
	protected BigDecimal getWholeDollarsOver(BigDecimal amount, TaxableIncome taxableIncome, TaxRate taxRate) {
		BigDecimal wholeDollarsOver = BigDecimal.ZERO;
		final BigDecimal additionalTaxAmount = taxRate.getAdditionalTaxAmt();
		if (additionalTaxAmount != null 
				&& taxableIncome.getTaxableIncomeRange() != null 
				&& taxableIncome.getTaxableIncomeRange().getIncomeRangeStart() != null) {
			
			wholeDollarsOver = amount.subtract(taxableIncome.getTaxableIncomeRange().getIncomeRangeStart().subtract(BigDecimal.ONE));
		}
		return wholeDollarsOver;		
	}
	
	public TaxableIncome getTaxableIncome(BigDecimal amount) {
		final TaxableIncome taxableIncome = this.taxRatesPerYear.getTaxableIncomes().stream().filter(incomeTax -> {
			final BigDecimal incomeRangeStart = incomeTax.getTaxableIncomeRange().getIncomeRangeStart();
			final BigDecimal incomeRangeEnd = incomeTax.getTaxableIncomeRange().getIncomeRangeEnd();
			
			boolean inRange = false;
			if (incomeRangeStart != null) {
				inRange = incomeRangeStart.compareTo(amount) <= 0;
			}
			if (incomeRangeEnd != null) {
				inRange = inRange && incomeRangeEnd.compareTo(amount) >= 0;
			}
			return inRange;
		})
		.findAny()
		.orElse(null);
		
		return taxableIncome;
	}



	

}
