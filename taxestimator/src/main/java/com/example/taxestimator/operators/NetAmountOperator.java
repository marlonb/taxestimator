package com.example.taxestimator.operators;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Net amount calculator class
 * 
 * @author Marlon.Bautista
 *
 */
public class NetAmountOperator implements IncomeTaxOperator {
	private BigDecimal incomeTaxAmount;

	public NetAmountOperator(BigDecimal incomeTaxAmount) {
		super();
		this.incomeTaxAmount = incomeTaxAmount;
	}


	@Override
	public BigDecimal calculate(BigDecimal amount) {
		BigDecimal netIncomeAmount = BigDecimal.ZERO;
		if (amount != null && this.incomeTaxAmount != null && amount.compareTo(this.incomeTaxAmount) >= 0) {
			netIncomeAmount = amount.subtract(this.incomeTaxAmount);
		} else {
			//log
		}
		return netIncomeAmount.setScale(2, RoundingMode.HALF_UP);	
				
	}
	
	



}
