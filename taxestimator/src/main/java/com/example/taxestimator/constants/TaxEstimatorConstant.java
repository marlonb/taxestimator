package com.example.taxestimator.constants;

import java.math.BigDecimal;

/**
 * App constants
 * 
 * @author Marlon.Bautista
 *
 */
public final class TaxEstimatorConstant {
	public static final String TAX_ESTIMATOR_APP_API_ENDPOINT = "/api";
	public static final String TAX_RATES_RESOURCE_API_ENDPOINT = "/api";
	public static final String TAX_RATES_RESOURCE_HOST_PROP_NAME = "TaxRatesResource.host";
	public static final String TAX_RATES_RESOURCE_PORT_PROP_NAME = "TaxRatesResource.port";
	public static final Double SUPER_ANNUATION_MIN_PERCENTAGE = new Double(9.50);
	public static final Double SUPER_ANNUATION_MAX_PERCENTAGE = new Double(25.00);
	public static final BigDecimal BASE_INCOME_FOR_SUPER_ELLIGIBILITY = new BigDecimal(5400.00);

	

	private TaxEstimatorConstant() {
		//private no-arg constructor
	}
	

}
