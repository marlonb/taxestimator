package com.example.taxestimator.constants;

import io.jsonwebtoken.SignatureAlgorithm;

/**
 * Security-related constants
 * 
 * @author Marlon.Bautista
 *
 */
public class SecurityConstant {
	public static final String APP_NAME = "TaxEstimatorApp";
	public static final String CUSTOM_LOGIN_URL = "/api/login";
	public static final String SECRET_KEY = "JWTSecretKeyInformation";
	public static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String AUTH_HEADER_KEY = "Authorization";
	
	private SecurityConstant() {
		//private no-arg constructor
		
	}

}
