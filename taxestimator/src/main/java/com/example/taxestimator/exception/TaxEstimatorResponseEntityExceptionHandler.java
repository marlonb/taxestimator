package com.example.taxestimator.exception;

import java.time.LocalDateTime;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.taxestimator.models.TaxEstimatorError;


/**
 * Application Exception Interceptor 
 * 
 * @author Marlon.Bautista
 *
 */
@ControllerAdvice
@RestController
public class TaxEstimatorResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		TaxEstimatorError errorDetails = new TaxEstimatorError(LocalDateTime.now(), 
				ex.getMessage(),
				request.getDescription(false));
		return new ResponseEntity(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(TaxEstimatorException.class)
	public final ResponseEntity<Object> handleTaxEstimatorException(TaxEstimatorException ex, WebRequest request) {
		TaxEstimatorError errorDetails = new TaxEstimatorError(LocalDateTime.now(), 
				ex.getMessage() != null && !ex.getMessage().isEmpty() ? ex.getMessage() : "",
				request.getDescription(false));
		
		return new ResponseEntity(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		final String errorMsg = ex.getBindingResult().getFieldErrors().stream()
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .findFirst()
                .orElse(ex.getMessage());
		
		
		TaxEstimatorError errorDetails = new TaxEstimatorError(LocalDateTime.now(), 
				"Validation Failed",
				errorMsg);

		return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
	} 

}
