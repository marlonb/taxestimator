package com.example.taxestimator.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.taxestimator.domain.TaxCalculationHistory;

/**
 * TransactionHistory repository
 * @author Marlon.Bautista
 *
 */
public interface TransactionHistoryRepository extends MongoRepository<TaxCalculationHistory, String> {
	/**
	 * Finds all calculation history for the given username.
	 * The result is ordered by Timestamp in descending order.
	 * 
	 * @param username The username to query with.
	 * @return Returns a list of all the tax calculation history for the given user.
	 * 
	 */
	List<TaxCalculationHistory> findByUsernameOrderByTimestampDesc(String username);
	
	/**
	 * Returns true 
	 * @param username
	 * @return
	 */
	//boolean existsByUsername(String username);
	
	/**
	 * Deletes all the tax calculation history for the given user.
	 * 
	 * @param username The user to delete all transactions for.
	 * @return the number of deleted records.
	 */
	long deleteByUsername(String username);
}