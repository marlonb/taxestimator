package com.example.taxestimator.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.taxestimator.domain.User;
/**
 * User repository
 * 
 * @author Marlon.Bautista
 *
 */
public interface UserRepository extends MongoRepository<User, String> {
	/**
	 * Queries the given user.
	 * 
	 * @param username The user.
	 * @return The user information
	 */
    User findByUsername(String username);
}