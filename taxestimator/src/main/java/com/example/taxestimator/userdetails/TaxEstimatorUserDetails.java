package com.example.taxestimator.userdetails;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.example.taxestimator.domain.User;

/** 
 * Spring security UserDetails model
 * 
 * @author Marlon.Bautista
 *
 */
public class TaxEstimatorUserDetails implements UserDetails {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4012653149564084410L;
	
	private User user;	

	public TaxEstimatorUserDetails(User user) {
		super();
		this.user = user;
	}
	
	

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getPassword() {
		return this.user.getPassword();
	}

	@Override
	public String getUsername() {
		return this.user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
	
	

}
