package com.example.taxestimator.userdetails;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.example.taxestimator.domain.User;
import com.example.taxestimator.repositories.UserRepository;


/** 
 * The Spring security  UserDetailsService implementation
 * 
 * @author Marlon.Bautista
 *
 */
@Component
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private UserRepository repository;
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		final User user = repository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        } else {
            final UserDetails details = new TaxEstimatorUserDetails(user);
            return details;
        }        
        
	}

}
