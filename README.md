# Tax Estimator Application

## Quick Information

### Project Requirements

 - Front-End: 
 https://bitbucket.org/marlonb/taxestimator/src/master/fron-end-requirement.JPG
  
 - Back-End:
 https://bitbucket.org/marlonb/taxestimator/src/master/back-end-requirement.JPG

### AWS Deployment

 - Tax Rates Resource API:
 http://taxestimatorresource-env.6pcrgm3s3m.us-east-1.elasticbeanstalk.com/api
 - Tax Estimator App:
 http://taxestimator.6pcrgm3s3m.us-east-1.elasticbeanstalk.com

### Pre-defined Users (user/password):
 
 user1/user1
 
 user2/user2
 
 user3/user3
 
 user4/user4
 
 user5/user5
 
 user6/user6
 
 

# Database
The application data are persisted to a MongoDb instance on mLab (https://mlab.com/welcome/), a database-as-a-service for MongoDb provider. The database uri is: mongodb://dbuser:dbuser@ds235609.mlab.com:35609/taxcalcdb.
# Tax Rates Models
This project contains the reusable models used by both the Tax Rates Resource and Tax Estimator Application projects.

# Tax Rates Resource
An external REST API that provides tax rates data. The application is running on Spring Boot.

# Tax Estimator
The back-end service, running on Spring Boot that performs the different calculations based from the received TaxEstimatorRequest.

 - Models:
	 - TaxEstimatorRequest - The request object model.
	 - TaxEstimatorResponse - The response object model.
 - TaxRatesCacheService - Runs a background task every 5 minutes to fetch tax rates data from Tax Rates Resource API.
 - Operators: IncomeTaxOperator classes perform the actual calculations in the calculate() method.
	 - GrossAmountOperator
	 - GrossPlusSuperAmountOperator
	 - NetAmountOperator
	 - NetIncomePlusSuperAmountOperator
	 - SuperAnnuationOperator - Super annuation amount has upper limit restrictions based on:
	   https://www.superguide.com.au/boost-your-superannuation/upper-limit-on-sg-contributions
	 - TaxAmountOperator
 - AbstractTaxCalculator - The base class that drives the calculation process by calling the different operators.
 - Bean Validation API - The application leverages on predefined (and custom validators) from Java BeanValidation API to validate the received request.
 - Spring Security (JWT Tokens) - The application is secured and users are authenticated using a Token retrieved from the Authorization header of the HTTP request.

	Example: The token is generated by sending POST request to http://localhost:8080/api/login endpoint with the content as: {"username": "user1", "password": "user1"}

	Succeeding requests should send the generated token in the Authorization header.
    
 - TaxEstimatorController - The REST API controller.

# Tax Estimator UI
The Tax Estimator App front-end.

Angular: 5.2.9

@angular/material: 5.2.4

@angular/flex-layout: 5.0.0-beta.14

# Running the app locally

1. Run the provided Tax Estimator Resource Jar: java -jar taxratesresource-0.0.1-SNAPSHOT.jar
2. Run the provided Tax Estimator Application Jar: java -jar taxestimator-0.0.1-SNAPSHOT.jar

The Tax Rates Resource endpoint is: http://localhost:9000/api

The application is at: http://localhost:8080
