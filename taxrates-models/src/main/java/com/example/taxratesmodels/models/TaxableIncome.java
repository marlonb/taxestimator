package com.example.taxratesmodels.models;

import java.io.Serializable;

public class TaxableIncome implements Serializable {
	/*
	 * 
	 */
	private static final long serialVersionUID = 2458515470029435332L;
	
	private TaxableIncomeRange taxableIncomeRange;
	private TaxRate taxRate;
	
	
	
	
	public TaxableIncome() {
		super();
		// TODO Auto-generated constructor stub
	}


	public TaxableIncome(TaxableIncomeRange taxableIncomeRange, TaxRate taxRate) {
		super();
		this.taxableIncomeRange = taxableIncomeRange;
		this.taxRate = taxRate;
	}
	
	
	public TaxableIncomeRange getTaxableIncomeRange() {
		return taxableIncomeRange;
	}
	public TaxRate getTaxRate() {
		return taxRate;
	}
	
	
	
	
	

	 
	

}
