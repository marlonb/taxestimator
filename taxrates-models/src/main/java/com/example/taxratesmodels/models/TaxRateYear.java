package com.example.taxratesmodels.models;

public class TaxRateYear {
	private int year;

	
	public TaxRateYear() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TaxRateYear(int year) {
		super();
		this.year = year;	
		
	}

	public int getYear() {
		return year;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxRateYear other = (TaxRateYear) obj;
		if (year != other.year)
			return false;
		return true;
	}



	
	

}
