package com.example.taxratesmodels.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class TaxableIncomeRange implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9204224923298388185L;
	
	private BigDecimal incomeRangeStart;
	private BigDecimal incomeRangeEnd;
	
	
	public TaxableIncomeRange() {
		super();
		// TODO Auto-generated constructor stub
	}


	public TaxableIncomeRange(BigDecimal incomeRangeStart, BigDecimal incomeRangeEnd) {
		super();
		this.incomeRangeStart = incomeRangeStart != null ? incomeRangeStart.setScale(2, RoundingMode.HALF_UP) : incomeRangeStart;
		this.incomeRangeEnd = incomeRangeEnd != null ? incomeRangeEnd.setScale(2, RoundingMode.HALF_UP) : incomeRangeEnd;
	}


	public BigDecimal getIncomeRangeStart() {
		return incomeRangeStart;
	}


	public BigDecimal getIncomeRangeEnd() {
		return incomeRangeEnd;
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((incomeRangeEnd == null) ? 0 : incomeRangeEnd.hashCode());
		result = prime * result + ((incomeRangeStart == null) ? 0 : incomeRangeStart.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxableIncomeRange other = (TaxableIncomeRange) obj;
		if (incomeRangeEnd == null) {
			if (other.incomeRangeEnd != null)
				return false;
		} else if (!incomeRangeEnd.equals(other.incomeRangeEnd))
			return false;
		if (incomeRangeStart == null) {
			if (other.incomeRangeStart != null)
				return false;
		} else if (!incomeRangeStart.equals(other.incomeRangeStart))
			return false;
		return true;
	}




	
	

}
