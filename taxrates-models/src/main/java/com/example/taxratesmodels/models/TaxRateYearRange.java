package com.example.taxratesmodels.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class TaxRateYearRange implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = -1195657152317847065L;
	
	private TaxRateYear taxRateYearStart;
	private TaxRateYear taxRateYearEnd;

	//based on https://www.superguide.com.au/boost-your-superannuation/upper-limit-on-sg-contributions
	private BigDecimal maxSuperContributionAmount;
	
	
	
	public TaxRateYearRange() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public TaxRateYearRange(TaxRateYear taxRateYearStart, TaxRateYear taxRateYearEnd) {
		//super();
		//this.taxRateYearStart = taxRateYearStart;
		//this.taxRateYearEnd = taxRateYearEnd;
		this(taxRateYearStart, taxRateYearEnd, null);
		
	}
	
	

	public TaxRateYearRange(TaxRateYear taxRateYearStart, TaxRateYear taxRateYearEnd, BigDecimal maxSuperContributionAmount) {
		super();
		this.taxRateYearStart = taxRateYearStart;
		this.taxRateYearEnd = taxRateYearEnd;
		this.maxSuperContributionAmount = maxSuperContributionAmount != null ? maxSuperContributionAmount.setScale(2, RoundingMode.HALF_UP) : maxSuperContributionAmount;
		
		if (this.taxRateYearStart.getYear() > this.taxRateYearEnd.getYear()) {
			throw new IllegalArgumentException("Invalid year start and/or year end range");
		}
	}
	
	public TaxRateYear getTaxRateYearStart() {
		return taxRateYearStart;
	}
	public TaxRateYear getTaxRateYearEnd() {
		return taxRateYearEnd;
	}
	

	public BigDecimal getMaxSuperContributionAmount() {
		return maxSuperContributionAmount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((taxRateYearEnd == null) ? 0 : taxRateYearEnd.hashCode());
		result = prime * result + ((taxRateYearStart == null) ? 0 : taxRateYearStart.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxRateYearRange other = (TaxRateYearRange) obj;
		if (taxRateYearEnd == null) {
			if (other.taxRateYearEnd != null)
				return false;
		} else if (!taxRateYearEnd.equals(other.taxRateYearEnd))
			return false;
		if (taxRateYearStart == null) {
			if (other.taxRateYearStart != null)
				return false;
		} else if (!taxRateYearStart.equals(other.taxRateYearStart))
			return false;
		return true;
	}

	
	
	

	
	
	
	
	
	
	
	

}
