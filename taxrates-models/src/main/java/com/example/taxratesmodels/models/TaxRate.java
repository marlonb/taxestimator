package com.example.taxratesmodels.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class TaxRate implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 6240644209362258044L;
	
	private TaxRateYearRange taxRateYear;
	private BigDecimal baseTaxAmt;
	private BigDecimal additionalTaxAmt;
	
	
	
	public TaxRate() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TaxRate(TaxRateYearRange taxRateYear, BigDecimal baseTaxAmt, BigDecimal additionalTaxAmt) {
		super();
		this.taxRateYear = taxRateYear;
		this.baseTaxAmt = baseTaxAmt != null ? baseTaxAmt.setScale(2, RoundingMode.HALF_UP) : baseTaxAmt;
		this.additionalTaxAmt = additionalTaxAmt != null ? additionalTaxAmt.setScale(3, RoundingMode.HALF_DOWN) : additionalTaxAmt;
	}

	public TaxRateYearRange getTaxRateYear() {
		return taxRateYear;
	}

	public BigDecimal getBaseTaxAmt() {
		return baseTaxAmt;
	}

	public BigDecimal getAdditionalTaxAmt() {
		return additionalTaxAmt;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((additionalTaxAmt == null) ? 0 : additionalTaxAmt.hashCode());
		result = prime * result + ((baseTaxAmt == null) ? 0 : baseTaxAmt.hashCode());
		result = prime * result + ((taxRateYear == null) ? 0 : taxRateYear.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxRate other = (TaxRate) obj;
		if (additionalTaxAmt == null) {
			if (other.additionalTaxAmt != null)
				return false;
		} else if (!additionalTaxAmt.equals(other.additionalTaxAmt))
			return false;
		if (baseTaxAmt == null) {
			if (other.baseTaxAmt != null)
				return false;
		} else if (!baseTaxAmt.equals(other.baseTaxAmt))
			return false;
		if (taxRateYear == null) {
			if (other.taxRateYear != null)
				return false;
		} else if (!taxRateYear.equals(other.taxRateYear))
			return false;
		return true;
	}
	


	
	
	
	

	

}
