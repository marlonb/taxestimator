package com.example.taxratesmodels.models;

import java.io.Serializable;
import java.util.List;

public class TaxRatesPerYear implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2510761266831263024L;
	
	private TaxRateYearRange taxRateYearRange;
	private List<TaxableIncome> taxableIncomes;
	
	
	public TaxRatesPerYear() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TaxRatesPerYear(TaxRateYearRange taxRateYearRange, List<TaxableIncome> taxableIncomes) {
		super();
		this.taxRateYearRange = taxRateYearRange;
		this.taxableIncomes = taxableIncomes;
	}

	public TaxRateYearRange getTaxRateYearRange() {
		return taxRateYearRange;
	}

	public List<TaxableIncome> getTaxableIncomes() {
		return taxableIncomes;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((taxRateYearRange == null) ? 0 : taxRateYearRange.hashCode());
		result = prime * result + ((taxableIncomes == null) ? 0 : taxableIncomes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaxRatesPerYear other = (TaxRatesPerYear) obj;
		if (taxRateYearRange == null) {
			if (other.taxRateYearRange != null)
				return false;
		} else if (!taxRateYearRange.equals(other.taxRateYearRange))
			return false;
		if (taxableIncomes == null) {
			if (other.taxableIncomes != null)
				return false;
		} else if (!taxableIncomes.equals(other.taxableIncomes))
			return false;
		return true;
	}
	
	
	
	
	

}
