package com.example.taxratesresource.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.taxratesmodels.models.TaxRatesPerYear;
import com.example.taxratesresource.services.IncomeTaxRatesGeneratorService;


/**
 * The Tax rates resource endpoints
 * 
 * @author Marlon.Bautista
 *
 */
@RestController
@RequestMapping("/api")
public class TaxRatesController {
	@Autowired
	private IncomeTaxRatesGeneratorService incomeTaxGenerator;
	

	@GetMapping()
	public List<TaxRatesPerYear> getTaxRates() {
		return incomeTaxGenerator.generateResidentIncomeTaxRates();
	}

}
