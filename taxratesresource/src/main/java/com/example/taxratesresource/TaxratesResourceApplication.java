package com.example.taxratesresource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The main Tax rates resource Spring boot application
 * @author Marlon.Bautista
 *
 */
@SpringBootApplication
public class TaxratesResourceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaxratesResourceApplication.class, args);
	}
}
