package com.example.taxratesresource.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.taxratesmodels.models.TaxRate;
import com.example.taxratesmodels.models.TaxRateYear;
import com.example.taxratesmodels.models.TaxRateYearRange;
import com.example.taxratesmodels.models.TaxRatesPerYear;
import com.example.taxratesmodels.models.TaxableIncome;
import com.example.taxratesmodels.models.TaxableIncomeRange;


@Service
/**
 * Generates tax rates. 
 * 
 * Sources: 
 * https://www.ato.gov.au/Rates/Individual-income-tax-for-prior-years/
 * https://www.ato.gov.au/Rates/Individual-income-tax-rates/
 * 
 * @author Marlon.Bautista
 *
 */
public class SimpleIncomeTaxRatesGenerator implements IncomeTaxRatesGeneratorService {

	@Override
	public List<TaxRatesPerYear> generateResidentIncomeTaxRates() {
		final List<TaxRatesPerYear> incomeTaxRatesPerYear = new ArrayList<>(6);

		TaxableIncomeRange incomeRange1 = new TaxableIncomeRange(new BigDecimal(0.00), new BigDecimal(18200.00));
		TaxableIncomeRange incomeRange2 = new TaxableIncomeRange(new BigDecimal(18201.00), new BigDecimal(37000.00));
		TaxableIncomeRange incomeRange3 = new TaxableIncomeRange(new BigDecimal(37001.00), new BigDecimal(87000.00));
		TaxableIncomeRange incomeRange4 = new TaxableIncomeRange(new BigDecimal(87001.00), new BigDecimal(180000.00));
		TaxableIncomeRange incomeRange5 = new TaxableIncomeRange(new BigDecimal(180001.00), null);

		//2017-2018
		List<TaxableIncome> taxableIncomes = new ArrayList<>(5);		
		TaxRateYearRange yearRange = new TaxRateYearRange(new TaxRateYear(2017), new TaxRateYear(2018), new BigDecimal(20048.80));
		TaxRate taxRate2 = new TaxRate(yearRange, new BigDecimal(0.00), new BigDecimal(.19));
		TaxRate taxRate3 = new TaxRate(yearRange, new BigDecimal(3572.00), new BigDecimal(.325));
		TaxRate taxRate4 = new TaxRate(yearRange, new BigDecimal(19822.00), new BigDecimal(.37));
		TaxRate taxRate5 = new TaxRate(yearRange, new BigDecimal(54232.00), new BigDecimal(.45));		
		taxableIncomes.add(new TaxableIncome(incomeRange1, null));
		taxableIncomes.add(new TaxableIncome(incomeRange2, taxRate2));
		taxableIncomes.add(new TaxableIncome(incomeRange3, taxRate3));
		taxableIncomes.add(new TaxableIncome(incomeRange4, taxRate4));
		taxableIncomes.add(new TaxableIncome(incomeRange5, taxRate5));

		TaxRatesPerYear taxRatesPerYear = new TaxRatesPerYear(yearRange, taxableIncomes);
		incomeTaxRatesPerYear.add(taxRatesPerYear);	




		//2016-2017
		taxableIncomes = new ArrayList<>(5);		
		yearRange = new TaxRateYearRange(new TaxRateYear(2016), new TaxRateYear(2017), new BigDecimal(19615.60));
		taxRate2 = new TaxRate(yearRange, new BigDecimal(0.00), new BigDecimal(.19));
		taxRate3 = new TaxRate(yearRange, new BigDecimal(3572.00), new BigDecimal(.325));
		taxRate4 = new TaxRate(yearRange, new BigDecimal(19822.00), new BigDecimal(.37));
		taxRate5 = new TaxRate(yearRange, new BigDecimal(54232.00), new BigDecimal(.45));		
		taxableIncomes.add(new TaxableIncome(incomeRange1, null));
		taxableIncomes.add(new TaxableIncome(incomeRange2, taxRate2));
		taxableIncomes.add(new TaxableIncome(incomeRange3, taxRate3));
		taxableIncomes.add(new TaxableIncome(incomeRange4, taxRate4));
		taxableIncomes.add(new TaxableIncome(incomeRange5, taxRate5));

		taxRatesPerYear = new TaxRatesPerYear(yearRange, taxableIncomes);
		incomeTaxRatesPerYear.add(taxRatesPerYear);


		//2015-2016		
		incomeRange1 = new TaxableIncomeRange(new BigDecimal(0.00), new BigDecimal(18200.00));
		incomeRange2 = new TaxableIncomeRange(new BigDecimal(18201.00), new BigDecimal(37000.00));
		incomeRange3 = new TaxableIncomeRange(new BigDecimal(37001.00), new BigDecimal(80000.00));
		incomeRange4 = new TaxableIncomeRange(new BigDecimal(80001.00), new BigDecimal(180000.00));
		incomeRange5 = new TaxableIncomeRange(new BigDecimal(180001.00), null);


		taxableIncomes = new ArrayList<>(5);		
		yearRange = new TaxRateYearRange(new TaxRateYear(2015), new TaxRateYear(2016), new BigDecimal(19307.80));
		taxRate2 = new TaxRate(yearRange, new BigDecimal(0.00), new BigDecimal(.19));
		taxRate3 = new TaxRate(yearRange, new BigDecimal(3572.00), new BigDecimal(.325));
		taxRate4 = new TaxRate(yearRange, new BigDecimal(17547.00), new BigDecimal(.37));
		taxRate5 = new TaxRate(yearRange, new BigDecimal(54547.00), new BigDecimal(.45));		
		taxableIncomes.add(new TaxableIncome(incomeRange1, null));
		taxableIncomes.add(new TaxableIncome(incomeRange2, taxRate2));
		taxableIncomes.add(new TaxableIncome(incomeRange3, taxRate3));
		taxableIncomes.add(new TaxableIncome(incomeRange4, taxRate4));
		taxableIncomes.add(new TaxableIncome(incomeRange5, taxRate5));

		taxRatesPerYear = new TaxRatesPerYear(yearRange, taxableIncomes);
		incomeTaxRatesPerYear.add(taxRatesPerYear);


		//2014-2015
		taxableIncomes = new ArrayList<>(5);		
		yearRange = new TaxRateYearRange(new TaxRateYear(2014), new TaxRateYear(2015), new BigDecimal(18783.00));
		taxRate2 = new TaxRate(yearRange, new BigDecimal(0.00), new BigDecimal(.19));
		taxRate3 = new TaxRate(yearRange, new BigDecimal(3572.00), new BigDecimal(.325));
		taxRate4 = new TaxRate(yearRange, new BigDecimal(17547.00), new BigDecimal(.37));
		taxRate5 = new TaxRate(yearRange, new BigDecimal(54547.00), new BigDecimal(.45));	
		taxableIncomes.add(new TaxableIncome(incomeRange1, null));
		taxableIncomes.add(new TaxableIncome(incomeRange2, taxRate2));
		taxableIncomes.add(new TaxableIncome(incomeRange3, taxRate3));
		taxableIncomes.add(new TaxableIncome(incomeRange4, taxRate4));
		taxableIncomes.add(new TaxableIncome(incomeRange5, taxRate5));

		taxRatesPerYear = new TaxRatesPerYear(yearRange, taxableIncomes);
		incomeTaxRatesPerYear.add(taxRatesPerYear);


		//2013-2014
		taxableIncomes = new ArrayList<>(5);		
		yearRange = new TaxRateYearRange(new TaxRateYear(2013), new TaxRateYear(2014), new BigDecimal(17775.00));
		taxRate2 = new TaxRate(yearRange, new BigDecimal(0.00), new BigDecimal(.19));
		taxRate3 = new TaxRate(yearRange, new BigDecimal(3572.00), new BigDecimal(.325));
		taxRate4 = new TaxRate(yearRange, new BigDecimal(17547.00), new BigDecimal(.37));
		taxRate5 = new TaxRate(yearRange, new BigDecimal(54547.00), new BigDecimal(.45));	
		taxableIncomes.add(new TaxableIncome(incomeRange1, null));
		taxableIncomes.add(new TaxableIncome(incomeRange2, taxRate2));
		taxableIncomes.add(new TaxableIncome(incomeRange3, taxRate3));
		taxableIncomes.add(new TaxableIncome(incomeRange4, taxRate4));
		taxableIncomes.add(new TaxableIncome(incomeRange5, taxRate5));

		taxRatesPerYear = new TaxRatesPerYear(yearRange, taxableIncomes);
		incomeTaxRatesPerYear.add(taxRatesPerYear);



		//2012-2013
		taxableIncomes = new ArrayList<>(5);		
		yearRange = new TaxRateYearRange(new TaxRateYear(2012), new TaxRateYear(2013), new BigDecimal(17775.00));
		taxRate2 = new TaxRate(yearRange, new BigDecimal(0.00), new BigDecimal(.19));
		taxRate3 = new TaxRate(yearRange, new BigDecimal(3572.00), new BigDecimal(.325));
		taxRate4 = new TaxRate(yearRange, new BigDecimal(17547.00), new BigDecimal(.37));
		taxRate5 = new TaxRate(yearRange, new BigDecimal(54547.00), new BigDecimal(.45));	
		taxableIncomes.add(new TaxableIncome(incomeRange1, null));
		taxableIncomes.add(new TaxableIncome(incomeRange2, taxRate2));
		taxableIncomes.add(new TaxableIncome(incomeRange3, taxRate3));
		taxableIncomes.add(new TaxableIncome(incomeRange4, taxRate4));
		taxableIncomes.add(new TaxableIncome(incomeRange5, taxRate5));

		taxRatesPerYear = new TaxRatesPerYear(yearRange, taxableIncomes);
		incomeTaxRatesPerYear.add(taxRatesPerYear);
		
		





		return incomeTaxRatesPerYear;
	}

}
