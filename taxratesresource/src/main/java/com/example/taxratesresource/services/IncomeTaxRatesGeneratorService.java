package com.example.taxratesresource.services;

import java.util.List;

import com.example.taxratesmodels.models.TaxRatesPerYear;

/**
 * The tax rates generator service
 * 
 * @author Marlon.Bautista
 *
 */
public interface IncomeTaxRatesGeneratorService {
	
	
	List<TaxRatesPerYear> generateResidentIncomeTaxRates();

}
