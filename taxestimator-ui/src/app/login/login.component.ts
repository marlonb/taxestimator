import { Component, OnInit } from '@angular/core';
import { FormControl, FormArray, FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from "./../shared/services/authentication.service";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  //providers : [AuthenticateService]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  //user = {username: '', password: ''};
  errorMsg = '';
  loading = false;
  //returnUrl: string;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private fb: FormBuilder) {
    this.createForm();
  }
  //constructor() { }
  ngOnInit() {
    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    this.authenticationService.isAuthenticated.skip(1).subscribe(
      (isAuthenticated) => {
        if (isAuthenticated) {
          let returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
          this.router.navigate([returnUrl]);
          this.errorMsg = '';
        } else {
          this.errorMsg = 'Invalid username or password!';
        }
      }
    )
  }

  createForm() {
    this.loginForm = this.fb.group({
      'username': new FormControl('', [Validators.required]),
      'password': new FormControl('', [Validators.required]),
    });
  }


  login() {
    this.authenticationService.authenticate(this.loginForm.value);
  }
}
