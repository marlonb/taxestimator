import { TaxableIncomeModel } from './';

export class TaxEstimatorResponseModel {
    superAnnuationAmt: number;
    grossAmt: number;
    grossAmtPlusSuperAmt: number;
    taxAmt: number;
    netAmt: number;
    netAmtPlusSuperAmt: number;
    relatedTaxRate: TaxableIncomeModel;

}