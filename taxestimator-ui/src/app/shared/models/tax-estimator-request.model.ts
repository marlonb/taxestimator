import { TaxRateYearRangeModel } from './tax-rate-year-range.model';

export class TaxEstimatorRequestModel {
    superAnnuationPercentage: number;
    income: number;
    taxRateYearRange: TaxRateYearRangeModel;
    incomeIncludesSuperAnnuation: boolean;
    
}