import { TaxRateYearRangeModel } from './';

export class TaxRateModel {
    taxRateYear: TaxRateYearRangeModel;
    baseTaxAmt: number;
    additionalTaxAmt: number;

}