import { TaxEstimatorRequestModel } from './';
import { TaxEstimatorResponseModel } from './';

export class TaxCalculationHistoryModel {
    timestamp: string;
    request: TaxEstimatorRequestModel;
    response: TaxEstimatorResponseModel;


}