export class TaxrateInformationModel {
    taxableIncome: string;
    taxOnThisIncome: string;
}