import { YearModel } from './';
export class TaxRateYearRangeModel {
    taxRateYearStart: YearModel;
    taxRateYearEnd: YearModel;
}
