
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { TaxCalculationHistoryModel } from '../models'



@Injectable()
export class TaxHistoryService {

  constructor(protected apiService: ApiService) {
  }

  getHistoryList(): Observable<TaxCalculationHistoryModel[]> {
    return this.apiService.get('/history');

  }

  deleteHistoryList(): Observable<any> {

    return this.apiService.delete('/history').map(data => {
      return data
    });

  }

}
