import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authenticationService: AuthenticationService, private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {


         if (this.authenticationService.authenticated) {
             return true;
         } else {
             this.router.navigate(['/login'], {
                queryParams: {
                     returnUrl: state.url
                 }
             });
             return false;
         }
    }
}