import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { environment } from '../../../environments/environment';
import { JwtService } from './jwt.service';
import { ApiService } from './api.service';

import { User } from '../models';

var users = [
    //new UserComponent('admin','admin'),
    //new UserComponent('admin1','admin1')
];

@Injectable()
export class AuthenticationService {
    private currentUserSubject = new BehaviorSubject<User>(new User());
    public currentUser = this.currentUserSubject.asObservable().distinctUntilChanged();


    private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
    public isAuthenticated = this.isAuthenticatedSubject.asObservable();
    authenticated: boolean = false;

    constructor(private http: HttpClient,
        private jwtService: JwtService) {

    }





    private getLoginPath() {
        return `${environment.api_url}/login`;
    }

    private setAuth(user: User) {
        
        this.authenticated = true;
        // Save JWT sent from server in localstorage
        this.jwtService.saveToken(user.token);
        // Set current user data into observable
        this.currentUserSubject.next(user);
        // Set isAuthenticated to true
        this.isAuthenticatedSubject.next(true);
    }

    private purgeAuth() {
        this.authenticated = false;
        // Remove JWT from localstorage
        this.jwtService.destroyToken();
        // Set current user to an empty object
        this.currentUserSubject.next(new User());
        // Set auth status to false
        this.isAuthenticatedSubject.next(false);
        
    }




    authenticate(credentials) {
        this.http.post(this.getLoginPath(), 
            { username: credentials.username, password: credentials.password }, 
            { observe: 'response' })
        .subscribe(
            response => {
                let token = response.headers.get('Authorization');
                let username = response.body['username'];
                if (token && username) {
                    let user: User = {
                        username: username,
                        token: token
                    };
    
                    this.setAuth(user);
                } else {
                    this.isAuthenticatedSubject.next(false);
                }

            }, // success path
            error => {
                this.isAuthenticatedSubject.next(false);
            } // error path           
        );



    }



    logout() {
        this.purgeAuth();
    }

    
    get getCurrentUsername() {
        return this.currentUserSubject.value.username;
    }



}
