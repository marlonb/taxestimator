import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpParams, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Rx';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap, retry } from 'rxjs/operators';

import { ProgressIndicatorService } from './progress-indicator.service';
import { JwtService } from './jwt.service';

@Injectable()
export class ApiService {
  activeRequests: number = 0;
  constructor(
    private http: HttpClient,
    private jwtService: JwtService,
    private progressIndicatorService: ProgressIndicatorService
  ) { }

  private setHeaders(): HttpHeaders {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',     
    };

    if (this.jwtService.getToken()) {
      headersConfig['Authorization'] = `${this.jwtService.getToken()}`;
    }
    return new HttpHeaders(headersConfig);
  }

  private formatErrors(error: any) {
    return Observable.throw(error.json());
  }

  private getApiPath(path: string) {
    return `${environment.api_url}${path}`;
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    this.showLoader();
    return this.http.get(this.getApiPath(path), { headers: this.setHeaders(), params: params })
      .pipe(
        tap(res => this.log(`get request: ${path}`)),
        retry(5), // retry a failed request up to 3 times
        catchError(this.handleError))
      .finally(() => { this.hideLoader() });
  }


  put(path: string, body: Object = {}): Observable<any> {
    this.showLoader();
    return this.http.put(
        this.getApiPath(path),
        JSON.stringify(body),
        { headers: this.setHeaders() }) 
      .pipe(
        tap(_ => this.log(`put request: ${path}`)),
        retry(5), // retry a failed request up to 3 times
        catchError(this.handleError))
      .finally(() => { this.hideLoader() });
  }

  post(path: string, body: Object = {}): Observable<any> {
    this.showLoader();
    return this.http.post(
        this.getApiPath(path),
        JSON.stringify(body),
        { headers: this.setHeaders() })
      .pipe(
        tap(res => this.log(`post request: ${path}`)),
        retry(5), // retry a failed request up to 3 times
        catchError(this.handleError))
      .finally(() => { this.hideLoader() });
  }

  delete(path): Observable<any> {
    this.showLoader();
    return this.http.delete(
      this.getApiPath(path),
      { headers: this.setHeaders() })
      .pipe(
        retry(5), // retry a failed request up to 3 times
        catchError(this.handleError)
      )       
      .finally(() => { this.hideLoader() });
  }

  protected showLoader() {
    this.activeRequests++;
    this.handleLoader(this.activeRequests);
  }

  protected hideLoader() {
    this.activeRequests--;
    this.handleLoader(this.activeRequests);
  }

  private handleLoader(activeRequests: number) {
    if (activeRequests > 0) {
      this.progressIndicatorService.show();
    } else {
      this.progressIndicatorService.hide();
    }
  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };


  /** Log a HeroService message with the MessageService */
  private log(message: string) {
    console.log(message);
    //this.messageService.add('HeroService: ' + message);
  }
}
