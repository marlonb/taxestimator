import { CurrencyPipe } from '@angular/common';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { TaxrateInformationModel, TaxEstimatorResponseModel } from '../models'



@Injectable()
export class TaxrateInformationService {

    constructor() {
    }

    buildRelatedTaxRateInfo(responseModel: TaxEstimatorResponseModel, currencyPipe: CurrencyPipe): TaxrateInformationModel {
        let relatedTaxRateInfo: TaxrateInformationModel = { taxableIncome: '', taxOnThisIncome: '' };
        if (responseModel && responseModel.relatedTaxRate) {
            let incomeRangeStart = responseModel.relatedTaxRate.taxableIncomeRange.incomeRangeStart;
            let incomeRangeEnd = responseModel.relatedTaxRate.taxableIncomeRange.incomeRangeEnd;
    
            let taxableIncomeInfoStart = currencyPipe.transform(incomeRangeStart, 'AUD', 'symbol', '0.2-2');
    
            let taxableInfoEnd = '';
            if (incomeRangeEnd) {
                taxableInfoEnd = " - " + currencyPipe.transform(incomeRangeEnd, 'AUD', 'symbol', '0.2-2');
            } else {
                taxableInfoEnd = " and over";
            }
    
            this.buildRelatedTaxRateInfo['taxableIncome'] = taxableIncomeInfoStart + taxableInfoEnd;
    
    
            let taxOnThisIncome = '';
            if (responseModel.relatedTaxRate.taxRate) {
                if (responseModel.relatedTaxRate.taxRate.baseTaxAmt) {
                    taxOnThisIncome += currencyPipe.transform(responseModel.relatedTaxRate.taxRate.baseTaxAmt, 'AUD', 'symbol', '0.2-2');
                    taxOnThisIncome += " plus ";
                }
                if (responseModel.relatedTaxRate.taxRate.additionalTaxAmt) {
                    taxOnThisIncome += currencyPipe.transform(responseModel.relatedTaxRate.taxRate.additionalTaxAmt, 'AUD', 'symbol', '0.2-2');
                    taxOnThisIncome += " for each $1 over ";
                    taxOnThisIncome += (incomeRangeStart - 1);
                }
            } else {
                taxOnThisIncome = 'Nil';
            }
    
    
            relatedTaxRateInfo.taxableIncome = taxableIncomeInfoStart + taxableInfoEnd;
            relatedTaxRateInfo.taxOnThisIncome = taxOnThisIncome;

        }

        return relatedTaxRateInfo;
    }



}
