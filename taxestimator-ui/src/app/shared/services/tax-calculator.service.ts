
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { TaxRateYearRangeModel, TaxEstimatorRequestModel, TaxEstimatorResponseModel } from '../models'



@Injectable()
export class TaxCalculatorService {

  constructor(protected apiService: ApiService) {
  }

  getTaxRateYearRanges(): Observable<TaxRateYearRangeModel[]> {
    return this.apiService.get('/taxrates/yearranges');

  }

  calculate(request: TaxEstimatorRequestModel): Observable<TaxEstimatorResponseModel> {

    return this.apiService.post('/calculate', request).map(data => data);

  }

}
