import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ProgressIndicatorService, LoaderState } from '../../../shared/services';


@Component({
  selector: 'app-progress-indicator',
  templateUrl: './progress-indicator.component.html',
  styleUrls: ['./progress-indicator.component.css']
})
export class ProgressIndicatorComponent implements OnInit {

  show = false;

  private subscription: Subscription;

  constructor(
    private progressService: ProgressIndicatorService
  ) { }

  ngOnInit() {
    this.subscription = this.progressService.loaderState
      .subscribe((state: LoaderState) => {
        this.show = state.show;
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
