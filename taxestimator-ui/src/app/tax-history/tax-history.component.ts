import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { CurrencyPipe } from '@angular/common';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { MatDialog } from "@angular/material";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';
import { catchError, finalize } from "rxjs/operators";
import { of } from "rxjs/observable/of";
import { TaxCalculationHistoryModel, TaxrateInformationModel, TaxEstimatorResponseModel } from '../shared/models';
import { ConfirmationDialogComponent } from '../shared/components/confirm-dialog/confirm-dialog.component';
import { TaxHistoryService, TaxrateInformationService } from '../shared/services';

@Component({
  selector: 'tax-history',
  templateUrl: './tax-history.component.html',
  styleUrls: ['./tax-history.component.css'],
  providers: [CurrencyPipe]
})
export class TaxHistoryComponent implements OnInit {
  dataSource: HistoryDataSource;
  displayedColumns = ['timestamp',
    'requestTaxRateYearRage',
    'requestSuperPercentage',
    'requestIncome',
    'requestSuperIncluded',
    'responseSuperAmount',
    'responseGrossAmount',
    'responseGrossPlusSuperAmount',
    'responseTaxAmount',
    'responseNetAmount',
    'responseNetPlusSuperAmount',
    'responseTaxableIncome',
    'responseTaxOnThisIncome'
  ];


  constructor(
    private dialog: MatDialog,
    private router: Router,
    private taxHistoryService: TaxHistoryService,
    private taxrateInformationService: TaxrateInformationService,
    private currencyPipe: CurrencyPipe) {

  }

  ngOnInit() {
    this.dataSource = new HistoryDataSource(this.taxHistoryService);
    this.dataSource.loadHistories();

  }

  buildRelatedTaxRateInfo(responseModel: TaxEstimatorResponseModel): TaxrateInformationModel {

    return this.taxrateInformationService.buildRelatedTaxRateInfo(responseModel, this.currencyPipe);

  }

  refresh() {
    this.dataSource.loadHistories();
  }

  delete() {
    this.openDialog();
  }

  openDialog() {

    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {
        title: 'Delete Confirmation',
        message: 'Tax Calculation Histories will be permanently deleted. \
                  Are you sure you want to do this?' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dataSource.delete();
      }
    });
  }

}

export class HistoryDataSource extends DataSource<any> {
  private historySubject = new BehaviorSubject<TaxCalculationHistoryModel[]>([]);
  private deleteSubscription: Subscription;

  constructor(private taxHistoryService: TaxHistoryService) {
    super();

  }
  connect(): Observable<TaxCalculationHistoryModel[]> {
    return this.historySubject.asObservable();
  }
  disconnect() {
    this.historySubject.complete();

    if (this.deleteSubscription) {
      this.deleteSubscription.unsubscribe();
    }
  }


  delete() {
    this.deleteSubscription = this.taxHistoryService.deleteHistoryList()
      .pipe(
      catchError(() => of([])))
      .subscribe(data => this.loadHistories());
  }
  loadHistories() {
    this.taxHistoryService.getHistoryList()
      .pipe(
      catchError(() => of([])))
      .subscribe(histories => this.historySubject.next(histories));
  }



}