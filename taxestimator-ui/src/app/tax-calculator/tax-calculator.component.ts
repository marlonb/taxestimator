import { Component, OnInit } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { FormControl, FormArray, FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { TaxRateYearRangeModel, TaxEstimatorResponseModel, TaxrateInformationModel } from '../shared/models';
import { TaxCalculatorService, TaxrateInformationService } from '../shared/services';


@Component({
  selector: 'tax-calculator',
  templateUrl: './tax-calculator.component.html',
  styleUrls: ['./tax-calculator.component.css'],
  providers: [CurrencyPipe]
})
export class TaxCalculatorComponent implements OnInit {
  requestForm: FormGroup;
  response: TaxEstimatorResponseModel;
  yearRanges = [];
  relatedTaxRateInfo: TaxrateInformationModel = { taxableIncome: '', taxOnThisIncome: '' };

  constructor(private fb: FormBuilder,
    private service: TaxCalculatorService,
    private taxrateInformationService: TaxrateInformationService,
    private currencyPipe: CurrencyPipe) {
    this.createForm();


  }

  ngOnInit() {
    this.service.getTaxRateYearRanges().take(1).subscribe(
      (data) => {
        data.forEach(yearRange => {
          this.yearRanges.push(
            {
              value: yearRange,
              viewValue: `${yearRange.taxRateYearStart.year} - ${yearRange.taxRateYearEnd.year}`
            });
        }
        );
      });
  }

  createForm() {
    this.requestForm = this.fb.group({
      'taxRateYearRange': new FormControl('', [Validators.required]),
      'superAnnuationPercentage': new FormControl(9.5, [Validators.required, Validators.min(9.5), Validators.max(25)]),
      'income': new FormControl('', [Validators.required, Validators.min(0), Validators.max(99999999999)]),
      'incomeIncludesSuperAnnuation': new FormControl('')
    });
  }


  submitForm() {
    this.service
      .calculate(this.requestForm.value)
      .subscribe(
      response => {
        if (response) {
          console.log(response);
          this.response = response;
          this.relatedTaxRateInfo = this.buildRelatedTaxRateInfo(this.response);
        }

      },
      err => {

      }
      );

  }

  buildRelatedTaxRateInfo(response: TaxEstimatorResponseModel): TaxrateInformationModel {

    return this.taxrateInformationService.buildRelatedTaxRateInfo(this.response, this.currencyPipe);
  }



  get formControls(): { [key: string]: AbstractControl } {
    return this.requestForm.controls;
  }

}
