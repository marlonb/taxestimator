import { NgModule, Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {
  HttpClientModule,
  HttpClientXsrfModule,
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialAndLayouModule } from './material/materialandlayout.module';

//app module route declarations

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TaxCalculatorComponent } from './tax-calculator/tax-calculator.component';
import { TaxHistoryComponent } from './tax-history/tax-history.component';
import { LoginComponent } from './login/login.component';
import { ConfirmationDialogComponent } from './shared/components/confirm-dialog/confirm-dialog.component';


//services
import {
  AuthGuard,
  ProgressIndicatorService,
  AuthenticationService,
  JwtService,
  ApiService,
  TaxrateInformationService,
  TaxCalculatorService,
  TaxHistoryService
} from './shared/services';
import { ProgressIndicatorComponent } from './shared/components/progress-indicator/progress-indicator.component';



@NgModule({
  declarations: [
    AppComponent,
    TaxCalculatorComponent,
    TaxHistoryComponent,
    LoginComponent,
    ConfirmationDialogComponent,
    ProgressIndicatorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    //// Restore Xsrf support (Not Working)
    //HttpClientXsrfModule.withOptions({ cookieName: 'XSRF-TOKEN', headerName: 'X-XSRF-TOKEN' }),
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialAndLayouModule,
  ],
  providers: [
    AuthGuard,
    ProgressIndicatorService,
    AuthenticationService,
    JwtService,
    ApiService,
    TaxrateInformationService,
    TaxCalculatorService,
    TaxHistoryService,
 
  ],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmationDialogComponent]
})
export class AppModule { }
