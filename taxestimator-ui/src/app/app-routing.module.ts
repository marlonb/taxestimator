import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { TaxCalculatorComponent } from './tax-calculator/tax-calculator.component';
import { TaxHistoryComponent } from './tax-history/tax-history.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './shared/services';

const appRoutes: Routes = [
  { path: '', redirectTo: '/calculator', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'calculator', component: TaxCalculatorComponent, canActivate: [AuthGuard] }, 
  { path: 'history', component: TaxHistoryComponent, canActivate: [AuthGuard] },   
  { path: '**', redirectTo: '/calculator', pathMatch: 'full' } //always last
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        preloadingStrategy: PreloadAllModules,
        enableTracing: false
      } // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ],  
})
export class AppRoutingModule { }







