import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from "@angular/material";
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from './shared/services';
import { ConfirmationDialogComponent } from './shared/components/confirm-dialog/confirm-dialog.component';




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  navLinks: any[];
  title = 'app';
  authenticated: boolean = false;
  private authSub: Subscription;

  constructor(private router: Router,
    private dialog: MatDialog,
    private authenticationService: AuthenticationService, ) {
    this.navLinks = [
      { label: 'Tax Calculator', path: 'calculator' },
      { label: 'Tax Calculation History', path: 'history' }];

  }


  ngOnInit() {
    this.authSub = this.authenticationService.isAuthenticated.subscribe(
      (isAuthenticated) => {
        this.authenticated = isAuthenticated;
      }
    )
  }


  ngOnDestroy() {
    if (this.authSub) {
      this.authSub.unsubscribe();
    }
  }

  logout() {
    this.openDialog();
  }

  openDialog() {

    let dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '400px',
      data: {
        title: 'Logout Confirmation',
        message: 'Are you sure you want to log out?' }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
      }
    });
  }

  get user() {
    return this.authenticationService.getCurrentUsername;
  }
}
